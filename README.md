# Cartesian lattice counting #

This repository contains code and data to supplement the manuscript
"Cartesian lattice counting by the vertical 2-sum"
(J. Kohonen 2020; [arxiv](https://arxiv.org/abs/2007.03232)).  It contains:

* C code to generate modular/distributive/semimodular/geometric/graded lattices (gralist.c)
* C code to classify and count the lattices by symmetry type (latclass.c)
* C code to perform Cartesian counting for small n (latglue.c)
* SageMath code to perform Cartesian counting for large n
* Data from the counting

See also the OEIS sequences [A006981](https://oeis.org/A006981)
(number of modular lattices) and [A006982](https://oeis.org/A006982)
(number of distributive lattices).

### Required software
* [Nauty 2.7r1](http://pallini.di.uniroma1.it)
* [SageMath](https://www.sagemath.org)

### Compiling the C programs ###
* Get and compile Nauty
* Copy Nauty headers and library (nautyL1.a) to the code directory
* Edit makefile according to local environment
* make

### Example run

    $ ./classify-modulars 20                                        # Classify and count modulars of n<=20 elements, takes about 10 seconds
    ...
    header  n       sp      mf      ma      mc      mx      mh      bf      bs      tf      ts      cf      cs      cn      total
    glued   4       1       0       0       0       0       0       0       0       0       0       0       0       0       1
    glued   5       1       0       0       0       0       0       0       0       0       0       0       0       0       1
    glued   6       1       1       0       0       0       0       0       0       0       0       0       0       0       2
    ...
    glued   18      1426    1185    28      28      4       8       1250    52      1250    52      6156    84      4959    16482
    glued   19      3074    2664    61      61      5       11      2763    109     2763    109     14382   191     11736   37929
    glued   20      6783    6092    122     122     11      19      6267    207     6267    207     34236   457     27832   88622

    $ sagemath
    sage: attach("paper.sage")
    sage: xx = paper_read_piece_counts("glued.mod")                 # Read the piece counts we computed above (n<=20)
    sage: yy = paper_cartesian_count(xx,2000);                      # Perform Cartesian counting up to n=2000 elements to compute lower bounds
    sage: float(yy["cf"][2000] / yy["cf"][1999])                    # Ratio of last two lower bounds on CF-type compositions
    2.185387595743274
    sage: LL=list(lattices_from_graph6_file("lat.mod.20.g6"))       # Read the 20-element lattices we generated; takes a couple of minutes
    sage: len(LL)
    26097
    sage: sum(L.is_modular() for L in LL)
    26097                                                           # Yup, they are all modular


### Accompanying data ###
* data/glued.mod: numbers of modular vi-lattices (n<=35) by symmetry type
* data/glued.dist: numbers of distributive vi-lattices (n<=60) by symmetry type
* data/lat.mod.n: listing of modular vi-lattices (vertical 2-sums left out) in digraph6 format (n<=20)
* data/lat.dist.n: listing of distributive vi-lattices (vertical 2-sums left out) in digraph6 format (n<=20)

Small lattice listings (n<=20) are provided here for example purposes.
Bigger listings (modulars n<=35 and distributives n<=60) are in [Fairdata.fi](https://doi.org/10.23729/07dc2f70-7d34-4e25-98b3-2cd380c16019).

### Contacts ###

* For help contact Jukka Kohonen (jukka.kohonen@iki.fi)
