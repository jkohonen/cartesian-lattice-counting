/* undirect.c -- convert from digraph6 to graph6 format
   Based on copyg.c */

#define USAGE "undirect [infile [outfile]]"

#define HELPTEXT \
"  Convert from digraph6 to graph6 format, losing the edge directions.\n"

/***********************************************************************/

#define MAXN 60    /* Define this before including nauty.h */

#include "gtools.h"

int
main(int argc, char *argv[])
{
    graph *g = NULL;
    char *infilename,*outfilename;
    FILE *infile,*outfile;
    int codetype;
    int assumefixed = 0;
    long position = 0L;
    int m,n;
    boolean digraph;

    infilename = NULL;
    outfilename = NULL;
    if (argc >= 2)
	infilename = argv[1];
    if (argc >= 3)
	outfilename = argv[2];

    if (infilename && infilename[0] == '-') infilename = NULL;
    infile = opengraphfile(infilename,&codetype,assumefixed,position);
    if (!infile) exit(1);
    if (!infilename) infilename = "stdin";

    if (!outfilename || outfilename[0] == '-')
    {
        outfilename = "stdout";
        outfile = stdout;
    }
    else if ((outfile = fopen(outfilename,"w")) == NULL)
    {
        fprintf(stderr,"Can't open output file %s\n",outfilename);
        gt_abort(NULL);
    }

    while ((g = readgg(infile,NULL,0,&m,&n,&digraph)) != NULL)
    {
	writeg6(outfile,g,m,n);
	FREES(g);
    }

    exit(0);
}
