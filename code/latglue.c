// latglue.c -- Count vertical 2-sums of lattices
//
// Lattice counts are read from file or stdin.
// They must be in the format that latclass outputs.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

// Lattice classes
typedef enum
{
    unknowntype,
    sp,				/* specials: short or >=3 elts both ends */
    mf, ma, mc, mx, mh,		/* middle pieces, five subtypes */
    bf, bs,			/* bottom pieces, fixed or symm coatoms */
    tf, ts,			/* top pieces, fixed or symm atoms */
    cf, cs, cn,			/* compositions, fixed, symm or too-many coatoms */
    lattypelen,			/* pseudo for number of types */
} lattype;
const lattype lattypefirst = sp;
const lattype lattypelast  = cn;

const char* lattypes[] = {
    "unknowntype",
    "sp",
    "mf", "ma", "mc", "mx", "mh",
    "bf", "bs",
    "tf", "ts",
    "cf", "cs", "cn",
    "oops"
};

// Counters
#define MAXN 1000
int maxseen = 0;		/* largest lattice size seen */
long long latcount[MAXN][lattypelen];
bool seen[MAXN];


// Auxiliary functions

void die(const char* format, ...)
{
    va_list arglist;

    fprintf(stderr, "latglue: ");
    va_start(arglist, format);
    vfprintf(stderr, format, arglist);
    va_end(arglist);
    fprintf(stderr, "\n");

#if DEBUG
    int z = 1/0;		/* This triggers a break in gdb. */
#endif
    
    exit(1);
}

void usage()
{
    fprintf(stderr, "usage: latglue [infile]\n");
    exit(1);
}

void readcounts(FILE* in)
{
    char line[1000];
    int nfield;

    while (fgets(line, sizeof(line), in))
    {
	if (strncmp(line, "counts", 6) != 0)
	    continue;

	int n = 0;
	long long here[lattypelen];
	lattype typ;
	for (typ=lattypefirst; typ<=lattypelast; typ++)
	    here[typ] = 0;
	
	nfield = sscanf(line,
			"counts\t%d\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\t%lld\n",
			&n,
			here+sp, here+mf, here+ma, here+mc, here+mx, here+mh,
			here+bf, here+bs, here+tf, here+ts, here+cf, here+cs, here+cn);
	if (nfield != 14)
	    die("malformed line: %s", line);

	seen[n] = true;
	if (n > maxseen)
	    maxseen = n;
	for (typ=lattypefirst; typ<=lattypelast; typ++)
	    latcount[n][typ] += here[typ];
    }

    if (ferror(in))
	die("error reading input");
}

void printcounts()
{
    int n;
    lattype typ;
    
    for (n=6; n<=maxseen; n++)
    {
	printf("got\tn=%d", n);
	for (typ=lattypefirst; typ<=lattypelast; typ++)
	    printf("\t%lld", latcount[n][typ]);
	printf("\n");
    }
}

void checkmatch(int n, const char* name, long long gluecount, long long oldcount)
{
    if (oldcount>0 && gluecount!=oldcount)
    {
	printf("warning: n=%d %s mismatch glued %lld %s %lld\n",
	       n, name, gluecount,
	       (gluecount<oldcount ? "<" : ">"),
	       oldcount);
    }
}

void gluecounts()
{
    int n;			/* size of glued lattice */
    int m;			/* size of bottom part */
    int k;			/* size of addition */
    lattype typ;
    
    long long botfhere, botshere;
    long long cfhere, cshere, cnhere;
    long long cfold, csold, cnold;
    long long total;
    
    printf("header\tn");
    for (typ=lattypefirst; typ<=lattypelast; typ++)
	printf("\t%s", lattypes[typ]);
    printf("\ttotal");
    printf("\n");
	
    for (n=4; n<=maxseen; n++)
    {
	cfold = latcount[n][cf];
	csold = latcount[n][cs];
	cnold = latcount[n][cn];
	
	latcount[n][cf] = 0;
	latcount[n][cs] = 0;
	latcount[n][cn] = 0;
	total = 0;
	
	for (m=6; m<=n-2; m++)
	{
	    k = n-m+4;
	    if (k<6 || k>n)
		die("oops! k problem");

	    // Lower part of vertical 2-sum can be any of the following:
	    // middle part, bottom part, or extensible result of gluing
	    botfhere = latcount[m][mf] + latcount[m][ma]
		+ latcount[m][bf] + latcount[m][cf];
	    botshere = latcount[m][mc] + latcount[m][mx] + latcount[m][mh]
		+ latcount[m][bs] + latcount[m][cs];
	    
	    // CF results: Two fixed coatoms
	    cfhere = botfhere * (2*latcount[k][mf] + latcount[k][ma] + latcount[k][mh])
		+ botshere * (latcount[k][mf] + latcount[k][ma]);

	    // CS results: Two symmetric coatoms
	    cshere = botfhere * (2*latcount[k][mc] + latcount[k][mx])
		+ botshere * (latcount[k][mc] + latcount[k][mx] + latcount[k][mh]);

	    // CN results: Three or more coatoms, nonextensible
	    cnhere = botfhere * (2*latcount[k][tf] + latcount[k][ts])
		+ botshere * (latcount[k][tf] + latcount[k][ts]);

	    latcount[n][cf] += cfhere;
	    latcount[n][cs] += cshere;
	    latcount[n][cn] += cnhere;
	}

	printf("glued\t%d", n);
	for (typ=lattypefirst; typ<=lattypelast; typ++)
	{
	    printf("\t%lld", latcount[n][typ]);
	    total += latcount[n][typ];
	}
	printf("\t%lld", total);
	printf("\n");

	checkmatch(n, "cf", latcount[n][cf], cfold);
	checkmatch(n, "cs", latcount[n][cs], csold);
	checkmatch(n, "cn", latcount[n][cn], cnold);
    }
}

int main(int argc, char** argv)
{
    FILE* in;
    
    if (argc < 2)
    {
	in = stdin;
	readcounts(in);
    }
    else
    {
	while (argc >= 2)
	{
	    in = fopen(argv[1], "r");
	    if (!in)
		die("cannot open input file %s", argv[2]);
	    readcounts(in);
	    fclose(in);
	    argc--;
	    argv++;
	}
    }

    gluecounts();
    
    return 0;
}
