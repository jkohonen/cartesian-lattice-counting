/* gralist.c -- list graded lattices

   Author: Jukka Kohonen (jukka.kohonen@aalto.fi)
   June 2020
*/
long long count_hybrid;

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

#define MAXN 60    /* Define this before including nauty.h */
#include "nauty.h"
#include "naututil.h"
#include "naugroup.h"
#include "nautinv.h"
#include "gtools.h"

#if MAXM!=1
#error We very much assume that M==1
#endif

/****************************************************************************/
/* Data types and globals for lattice and hashtable storage */

/* Full-size lattice structure for search logic */
typedef struct
{
    int   n;			/* number of elements (excluding bottom) */
    graph H[MAXN*MAXM];		/* Hasse (covering relation) */
    graph U[MAXN*MAXM];		/* Upsets (transitive closure) */
    int   V[MAXN];		/* levels: start from coatoms=0. */
    graph RH[MAXN*MAXM];	/* Reverse Hasse (elements covered by given element) */
    int   pfirst;		/* index of first "parent" element */
    int   cfirst;		/* index of first "child" element */
    int   loners;		/* number of elements with up-degree equal to 1 */
    
    int   auttype;		/* 0=unknown, 1=fixed, 2=simple, 3=complex. */
    int   orbit[MAXN];		/* orbits of elements. Not valid if auttype unknown. */
    bool  havecanon;		/* true if we have a canonical version also */
    graph Hcanon[MAXN*MAXM];	/* canonical Hasse. Valid only if havecanon. */
} lattice;

/* Tree node that stores a small lattice structure for isomorphism checks */
typedef struct
{
    int32_t hash[3];		/* We know from naututil.c that hashes have at most 31 bits */
    int32_t left, right;	/* Child pointers in the binary tree */
    uint32_t n;			/* Size of lattice */
    graph H[MAXN*MAXM];		/* Hasse graph */
} latinfo;

/* Binary tree of small lattices. */
typedef struct
{
    long long nlat;			/* number of lattices stored */
    long long alloc;			/* allocated size (number of lattices that can be stored) */
    latinfo* lat;	  		/* allocated memory block (of tree nodes) */
} latstack;

/* Seeds for computing different hashes of lattices */
static int seed[3] = {2922320L, 19883109L, 489317L};

/* Stack for multiextend and decorate results */
#define LSTACK_SIZE 100
#define LSTACK_HASH 13
latstack Lstack[LSTACK_SIZE];
int Lstackptr;
long long Lstackpeak[LSTACK_SIZE];


/****************************************************************************/
/* Storage for statistics */

long long hashmismatch[3];
long long hashmismatchfull;

/* nauty calls are grouped and counted by time, in decades of nanoseconds.
   group 0 is <1 ns
   group 1 is <10 ns
   ...
   group 9 is <1 s
   group 10 is >=1 s
*/
#define STAT_NAUTY_MAXGROUP 10
long long stat_nauty_calls[STAT_NAUTY_MAXGROUP+1];
double stat_nauty_time[STAT_NAUTY_MAXGROUP+1];

#define STAT_TIMECLASS_MAX 150
double stat_timeclass_elapsed[STAT_TIMECLASS_MAX+1];
long long stat_timeclass_count[STAT_TIMECLASS_MAX+1];
int timeclass_current;
struct timespec timeclass_started;
void init_timeclass();
void switch_timeclass(int c, int count);
void finish_timeclass();


long long statn_calls;
long long statn_ngen;
long long statn_maxgen;

long long nlat_printed;
long long nlat_by_n[MAXN+1];
long long nlat_by_len[MAXN+1];

clock_t cpustart, cpufinish;
time_t  wallstart, wallfinish;

// Progress indicator at regular time intervals
int progress_every = 60;
time_t progress_next;		/* next timestamp (WALLCLOCK seconds) when progress should be displayed */
int current_root;
long long current_branch;
long long current_line;
long long progress_nlat;


/****************************************************************************/
/* Command line flags and arguments */

bool showstats  = false;        /* collect and show statistics */
bool output     = false;        /* print lattices */
bool onlymaxn   = false;        /* print only lattices of maximun size */
bool onlymaxlen = false;        /* print only lattices of maximum len */
bool type_semi         = false; /* only accept semimodular lattices */
bool type_lowersemi    = false; /* only accept lower semimodular lattices */
bool type_qsemi        = false; /* only accept quasi-semimodular lattices (semimodular above atoms) */
bool type_qlowersemi   = false; /* only accept quasi-lower semimodular lattices (lower semimodular below coatoms) */
bool type_indeco       = false; /* only accept vertically indecomposables */
bool type_coatomic     = false; /* only accept coatomics */
bool type_dist         = false; /* only accept distributives (at least try to) */

int nmax;                       /* max number of elements */
int lenmax;			/* max length of lattice (= rank of top = corank of bottom) */
int coatmin=1, coatmax=999;	/* min and max numbers of coatoms */
int minneck=1;			/* minimum width of neck levels (below coatoms, above atoms) */
int maxloners=999;		/* maximum number of updegree-1 elements */

int branchat=-1;		/* if branchat>0, stop recursion when we have elts >= branchat */
char branchfilename[50];	/* file where branch lattices are written to or read from */
FILE* branchfile = NULL;	/* output file for branches, if branchat>0 */
int branchoutputcount = 0;
int jobid=0, jobcount=1;
bool jobs = false; 		/* if true, read initial lattices from stdin and choose by jobid */


/****************************************************************************/
/* Nauty variables */

static DEFAULTOPTIONS_DIGRAPH(options);
static statsblk stats;

// Workpad for storing generators.
// Has to be global as userautomproc needs to access it.
static int Gtmp[MAXN*MAXN];
static int Glen;

/* Different kinds of automorphism groups.
   AFIXED means trivial group (all elements of interest are fixed).
   ASIMPLE means all elements are in symmetric boxes: within each box all permutations are automorphisms.
   AOTHER means anything more complicated.
*/
#define AUNKNOWN 0
#define AFIXED 1
#define ASIMPLE 2
#define AOTHER 3


/***************************************************************/
/* Function declarations */

void die(const char* format, ...);
    
static void userautomproc(int count, int *p, int *orbits, int numorbits, int stabvertex, int n);
void initnauty();
void callnauty(lattice* L);
int auttype(const lattice* L);

void pretty(FILE* f, const lattice* L);
void latticenode(lattice* L, int i);
void latticearc(lattice* L, int i, int j);
void copylattice(const lattice* from, lattice* to);
bool semicheck(const lattice* L, int k, const int* y);
bool semicheck_bottom(const lattice* L, int cfirst);
bool distcheck(const lattice* L, int k, const int* y);
bool distcheck_bottom(const lattice* L, int cfirst);
bool lowersemicheck(const lattice* L);

bool weakcomp_first(int m, int k, int* w);
bool weakcomp_next(int m, int k, int* w);
bool partition_first(int m, int k, int* w);
bool partition_next(int m, int k, int* w);
void decorate(const lattice* L, int M);
void multiextend(long long seq, lattice* L, int M);
void multiextend_helper(lattice* L, int k, bool haveprev, const int* xprev, set bitsprev,
			int ppneed, const lattice* Lur);
  

/***************************************************************/
/* Auxiliary functions */

#define assert(cond) do{ if(!(cond)) die("assert failed line=%d condition=[%s]\n", __LINE__, #cond); } while(0)
#define asserteq(lhs,rhs) do{ if((lhs)!=(rhs)) die("assert failed line=%d lhs=%g rhs=%g\n", __LINE__, (double)(lhs), (double)(rhs)); } while(0)

#define bitcount(n) (__builtin_popcountl(n))
#define ctz(n) (__builtin_ctzl(n))

void indent(int n)
{
    int i;
    for (i=0; i<n; i++)
	fprintf(stderr, "  ");
}

void debugstate(const lattice* L, const char* format, ...)
{
#if DEBUG >= 2
    va_list arglist;

    indent(L->n);
    va_start(arglist, format);
    vfprintf(stderr, format, arglist);
    va_end(arglist);
#endif
}

#if DEBUG >= 2
#define debug fprintf
#else
#define debug 0,
#endif
    
int numpairs(int n)
{
    return n*(n-1)/2;
}

void starttimer(struct timespec* from, double *elapsed)
{
    clock_gettime(CLOCK_MONOTONIC,from);
}
void stoptimer(struct timespec* from, double *elapsed)
{
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC,&now);
    *elapsed += 1.0*(now.tv_sec - from->tv_sec) + 1e-9*(now.tv_nsec - from->tv_nsec);
}

void init_timeclass()
{
    if (showstats)
    {
	int c;
	for (c=0; c<STAT_TIMECLASS_MAX; c++)
	{
	    stat_timeclass_elapsed[c] = 0.0;
	    stat_timeclass_count[c] = 0;
	}
	timeclass_current = 0;
	clock_gettime(CLOCK_MONOTONIC,&timeclass_started);
    }
}

void switch_timeclass(int c, int count)
{
    /* Accumulate time from current timeclass */
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC,&now);
    double elapsed = 1.0*(now.tv_sec - timeclass_started.tv_sec) + 1e-9*(now.tv_nsec - timeclass_started.tv_nsec);
    stat_timeclass_elapsed[c] += elapsed;
    stat_timeclass_count[c] += count;

    /* Switch to new class c */
    timeclass_current = c;
    timeclass_started = now;
}

void finish_timeclass()
{
    switch_timeclass(0,0);
}


void printclock()
{
    struct timeval tv;
    time_t nowtime;
    struct tm *nowtm;
    char buf[64];

    gettimeofday(&tv, NULL);
    nowtime = tv.tv_sec;
    nowtm = localtime(&nowtime);
    strftime(buf, sizeof buf, "%Y-%m-%d %H:%M:%S", nowtm);
    fprintf(stderr, "%s", buf);
}

double cpuspent()
{
    clock_t cpunow = clock();
    return (double)(cpunow-cpustart)/CLOCKS_PER_SEC;
}

long long wallspent()
{
    time_t wallnow = time(NULL);
    return wallnow-wallstart;
}

void init_progress()
{
    progress_next = time(NULL) + progress_every;
    wallstart = time(NULL);
    cpustart  = clock();
    progress_nlat = 0;
}

void progress(const lattice* L)
{
    time_t now = time(NULL);
    if (now >= progress_next)
    {
	printclock();
	fprintf(stderr, "  wall=%-6lld  cpu=%-8.1f", wallspent(), cpuspent());
	fprintf(stderr, "  delta=%-7lld", nlat_printed - progress_nlat);
	if (jobs)
	    fprintf(stderr, "  branch=%-5lld  line=%-7lld", current_branch, current_line);
	else
	    fprintf(stderr, "  root=%-2d", current_root);
	progress_nlat = nlat_printed;
	
	if (L)
	{
	    fprintf(stderr, "  now=");
	    writeg6(stderr, (graph*) L->H, MAXM, L->n);
	}
	else
	{
	    fprintf(stderr, "\n");
	}
	
	while (progress_next <= now)
	    progress_next += progress_every;
    }
}

int min(int a, int b)
{
    if (a<b)
	return a;
    else
	return b;
}

int max(int a, int b)
{
    if (a>b)
	return a;
    else
	return b;
}

/* For debugging only: Perform many validity checks on a lattice structure.
   In production code this does nothing. */
void superverify(const lattice* L)
{
#if DEBUG
    int n = L->n;
    int pfirst = L->pfirst;
    int cfirst = L->cfirst;
    int i,j;

    if (n<0 || n>MAXN)
	die("n out of range");
    
    if (!(0<=pfirst && pfirst<=cfirst && cfirst<=n))
	die("starters");
    
    for (i=0; i<n; i++)
    {
	bool isparent = (i>=L->pfirst && i<L->cfirst);
	bool ischild  = (i>=L->cfirst && i<n);
	int o = L->orbit[i];

	if (i==0 && L->V[i] != 0)
	    die("initial level");
	
	if (i>0 && L->V[i] < L->V[i-1])
	    die("decreasing level");

	if (isparent && L->V[i] != L->V[L->pfirst])
	    die("parents on different levels");

	if (ischild && i<n && L->V[i] != L->V[L->cfirst])
	    die("children on different levels");

	if (L->V[i]>0 && L->H[i]==0)
	    die("noncoatom lacks parents");
	    
	set tmp=L->H[i];
	while (tmp)
	{
	    TAKEBIT(j,tmp);
	    if (j > n)
		die("Hasse (%d,%d) too big", i, j);
	    if (!ISELEMENT(&L->RH[j], i))
		die("Hasse (%d,%d) but not reverse (%d,%d)", i, j, j, i);
	}
	
	tmp=L->RH[i];
	while (tmp)
	{
	    TAKEBIT(j,tmp);
	    if (j > n)
		die("Reverse Hasse (%d,%d) too big", i, j);
	    if (!ISELEMENT(&L->H[j], i))
		die("Reverse Hasse (%d,%d) but not Hasse (%d,%d)", i, j, j, i);
	}
	
	if (o<0 || o>n)
	    die("orbit number out of range");
	
	if (o > i)
	    die("orbit number mangled");

	if (L->auttype == AFIXED)
	{
	    if ((isparent || ischild) && o!=i)
		die("not really fixed");
	}

	if (o!=0 && L->V[i] != L->V[o])
	    die("orbit crosses levels");
    }
#endif
}


/***************************************************************/
/* Output functions */

/* Pretty-print the lattice (for visual inspection, debugging or statistics */
void pretty(FILE* f, const lattice* L)
{
    int i,j;
    int n = L->n;
    int pfirst = L->pfirst;
    int cfirst = L->cfirst;
    
    fprintf(f,"lattice n=%d", n);
    switch (L->auttype)
    {
    case AFIXED:  fprintf(f," aut=FIXED "); break;
    case ASIMPLE: fprintf(f," aut=SIMPLE"); break;
    case AOTHER:  fprintf(f," aut=OTHER "); break;
    default: fprintf(f," aut=???"); break;
    }
					     
    if (pfirst < cfirst)
	fprintf(f," par=%d:%d", pfirst, cfirst-1);
    if (cfirst < n)
	fprintf(f," chi=%d:%d", cfirst, n-1);
    fprintf(f,"\n");
    
    for (i=0; i<n; i++)
    {
	if (i>0 && L->V[i]!=L->V[i-1])
	    fprintf(f,"\n");
	fprintf(f," %2d", i);
	if (L->H[i])
	{
	    fprintf(f,"^");
	    bool first=true;
	    for (j=0; j<n; j++)
	    {
		if (ISELEMENT(&L->H[i], j))
		{
		    if (!first)
			fprintf(f,",");
		    fprintf(f,"%d",j);
		    first = false;
		}
	    }
	}
    }
    fprintf(f,"\n");

    fprintf(f,"orbit =");
    for (i=0; i<n; i++)
	fprintf(f," %2d", L->orbit[i]);
    fprintf(f,"\n");
}

void dpretty(const lattice* L)
{
    pretty(stderr, L);
}


/****************************************************************************/
/* Functions for the hashtables for isomorphism checking. */

long lathash(const lattice* L, int order)
{
    int32_t hash = hashgraph((graph*)L->Hcanon, MAXM, L->n, seed[order]);
    return hash;
}

void lathash_push()
{
    int i;
    Lstackptr++;
    if (Lstackptr >= LSTACK_SIZE)
	die("oops! stack overflow");
    Lstack[Lstackptr].nlat = 0;
}

void lathash_pop()
{
    if (Lstackptr <= 0)
	die("oops! stack underflow");

    if (Lstack[Lstackptr].alloc > 100000 && Lstack[Lstackptr].lat)
    {
	free(Lstack[Lstackptr].lat);
	Lstack[Lstackptr].lat   = NULL;
	Lstack[Lstackptr].alloc = 0;
	Lstack[Lstackptr].nlat  = 0;
    }

    Lstackptr--;
}

void lathashtotals()
{
    int i,j;
    long long grandtotal = 0;
    for (i=1; i<LSTACK_SIZE; i++)
    {
	long long sizehere = Lstackpeak[i];
	if (sizehere > 0)
	{
	    fprintf(stderr, "stack %d peak allocation = %lld entries (%.1f MB)\n",
		    i, sizehere, sizehere*sizeof(latinfo)/1000000.0);
	}
	grandtotal += sizehere;
    }
    fprintf(stderr, "stack total peak allocation = %lld entries (%.1f MB)\n",
	    grandtotal, grandtotal*sizeof(latinfo)/1000000.0);
}

bool findiso_and_store(const lattice* L, const char* context, int contextid)
{
    int i,j,k;
    int n = L->n;
    int32_t hash[3];
    uint32_t Hshifted[MAXN];
    int prev;
    bool goright;
    
    superverify(L);
    assert(L->havecanon);
    
    for (j=0; j<3; j++)
	hash[j] = lathash(L,j);
    
    latstack* e   = &Lstack[Lstackptr];
    int nlat      = e->nlat;
    latinfo* elat = e->lat;
    bool foundmismatch;

    prev = -1;
    i = 0;
    while (1)
    {
	if (i<0 || i>=nlat)
	    break;
	
	foundmismatch = false;
	
	for (j=0; j<3; j++)
	{
	    if (elat[i].hash[j] != hash[j])
	    {
		hashmismatch[j]++;
		foundmismatch = true;
		goright = hash[j] > elat[i].hash[j];
		break;
	    }
	}

	if (!foundmismatch)
	{
	    /* We still have to verify the actual edges */
	    if (elat[i].n != n || memcmp(elat[i].H, L->Hcanon, n*sizeof(graph)) != 0)
	    {
		hashmismatchfull++;
		foundmismatch = true;
		goright = true;
	    }
	}

	if (!foundmismatch)
	{
	    /* Found an isomorphic copy. We are done. */
	    return true;
	}

	/* Continue to next tree node. */
	prev = i;
	i = goright ? elat[i].right : elat[i].left;
    }

    /* It is a new nonisomorphic lattice, so append it to list, at nlat. */

    /* Make sure there is room. */
    if (e->nlat >= e->alloc)
    {
	long long newalloc = (e->alloc==0 ? 100 : 2*e->alloc);
	long long newsize = newalloc * sizeof(latinfo);
	
	if (newalloc > Lstackpeak[Lstackptr])
	{
	    if (newsize > 100000000)
	    {
		if (contextid > 0)
		{
		    fprintf(stderr, "stack[%d] peak grows to %lld lattices, %.1f MB for %s%d\n",
			    Lstackptr, newalloc, newsize/1.0e6, context, contextid);
		}
		else
		{
		    fprintf(stderr, "stack[%d] peak grows to %lld lattices, %.1f MB for %s\n",
			    Lstackptr, newalloc, newsize/1.0e6, context);
		}
	    }
	    Lstackpeak[Lstackptr] = newalloc;
	}
	
	e->lat = realloc(e->lat, newsize);
	if (!e->lat)
	    die("cannot realloc hash entry to %d\n", newalloc);
	elat = e->lat;
	e->alloc = newalloc;
    }
    
    /* Storing at nlat. Update tree. */
    if (prev != -1)
    {
	if (goright)
	    elat[prev].right = nlat;
	else
	    elat[prev].left  = nlat;
    }

    /* Then store it. */
    for (j=0; j<3; j++)
	e->lat[nlat].hash[j] = hash[j];
    elat[nlat].n = n;
    memcpy(elat[nlat].H, L->Hcanon, n*sizeof(graph));
    elat[nlat].left  = -1;
    elat[nlat].right = -1;
    e->nlat++;

    return false;
}


/****************************************************************************/
/* Interface to nauty */

void initnauty()
{
    /* verify we have compatible versions of the nauty routines. */
    nauty_check(WORDSIZE,MAXM,MAXN,NAUTYVERSIONID);

    // Change some options
    options.getcanon      = true;
    options.digraph       = true;
    options.defaultptn    = false;
    options.userautomproc = userautomproc;
    options.invarproc     = adjacencies;
    options.invararg      = 0;		/* default is 0 */
}

/* After nauty, interpret the general type of the automorphism group (wrt parent level).
   1 = fixed  = trivial group: no elements can move
   2 = simple = some elements are mutually symmetric (they can be permuted freely)
   3 = anything else (e.g. cyclic groups of order >= 3)

   This should be called right after nauty has written the generators in Gtmp.

   We use very simple heuristics, so it is possible to get a too "pessimistic"
   (high) result.
*/
int auttype(const lattice* L)
{
    int i,j;
    int n = L->n;
    int pfirst = L->pfirst;
    int cfirst = L->cfirst;
    if (Glen==0)
	return AFIXED;		/* No generators, so whole lattice is fixed. */
    bool anymove = false;
    for (i=0; i<Glen; i++)
    {
	/* How many parent elements does this generator move? 
           We only care about elements starting from pfirst and before cfirst. */
	int nmovep = 0;
	for (j=pfirst; j<cfirst; j++)
	{
	    if (Gtmp[i*n + j] != j)
		nmovep++;
	}

	if (nmovep > 2)
	    return AOTHER;
	if (nmovep > 0)
	    anymove = true;
    }

    if (anymove)
	return ASIMPLE;
    else
	return AFIXED;
}

/* This is called by nauty for each generator of the automorphism group. */
static void userautomproc(int count, int *p, int *orbits, int numorbits,
			  int stabvertex, int n)
{
    if (count != Glen+1 || Glen > n)
	die("internal error: n=%d Glen=%d count=%d", n, Glen, count);
    memcpy(Gtmp+Glen*n, p, n*sizeof(int));
    Glen++;
}

/* Call nauty, obtaining orbits, canonical labeling and generators. */
void callnauty(lattice* L)
{
    int ptn[MAXN];
    int canon[MAXN];
    int* orbit = L->orbit;
    int n = L->n;
    int i;

    /* Initial colors by level */
    for (i=0; i<n; i++)
    {
	canon[i] = i;
	ptn[i] = (i<n-1 && L->V[i]==L->V[i+1]);
	/* ptn[i] is true if this element does NOT end a color class,
	   that is, it is not last and it has same level as next elem. */
    }
    
    Glen = 0;			/* Empty list of generators */
    
    struct timespec nstart, nfinish;
    if (showstats)
	clock_gettime(CLOCK_MONOTONIC,&nstart);

    /* Call it! We trust it does not change the first parameter. */
    densenauty((graph*) L->H, canon, ptn, orbit, &options, &stats, MAXM, n, L->Hcanon);

    if (showstats)
    {
	clock_gettime(CLOCK_MONOTONIC,&nfinish);
	double elapsed = 1.0*(nfinish.tv_sec - nstart.tv_sec) + 1e-9*(nfinish.tv_nsec - nstart.tv_nsec);
	double limit = 1e-9;
	int group=0;
	while (group<STAT_NAUTY_MAXGROUP && elapsed>=limit)
	{
	    group++;
	    limit *= 10.0;
	}
	stat_nauty_calls[group]++;
	stat_nauty_time[group] += elapsed;
    }
    
    statn_calls++;
    statn_ngen += Glen;
    if (Glen>statn_maxgen)
	statn_maxgen=Glen;

    /* Analyze symmetry type */
    L->auttype = AUNKNOWN;
    L->auttype = auttype(L);

    /* We have the canonical form, returned by densenauty */
    L->havecanon = true;
}


/****************************************************************************/
/* Functions for inspecting lattice type (modularity etc.) */

/* Check semimodularity at proposed child.
   Any two parents must have common parent (possibly implied top).
   k   updegree of proposed child
   y   proposed parents (lattice element indexing)
*/
bool semicheck(const lattice* L, int k, const int* y)
{
    int i,j;

    assert(k>0);
    if (L->V[y[0]]==0)
	return true;		/* Trivially ok if parents are coatoms. */
    
    for (i=0; i<k; i++)
    {
	for (j=i+1; j<k; j++)
	{
	    set Hi = L->H[y[i]];
	    set Hj = L->H[y[j]];
	    if ((Hi&Hj) == 0)
	    {
		/* Failed semimodularity. */
		return false;
	    }
	}
    }
    return true;
}

bool semicheck_bottom(const lattice* L, int cfirst)
{
    int j,h;
    int n = L->n;

    for (j=cfirst; j<n; j++)
    {
	set Hj = L->H[j];
	for (h=j+1; h<n; h++)
	{
	    set Hh = L->H[h];
	    if ((Hj&Hh) == 0)
	    {
		return false;
	    }
	}
    }
    return true;
}

// Check local lattice condition at proposed child.
// It must not share two parents with any existing child.
//
// ybits   Proposd parents as bitset using lattice element indexing
//
bool locallattice(const lattice* L, set ybits)
{
    int i;
    
    for (i=L->cfirst; i<L->n; i++)
    {
	int ncommon = bitcount(L->H[i] & ybits);
	if (ncommon >= 2)
	    return false;
    }
    return true;
}


// Check global lattice condition at proposed child.
//
// ybits   Proposed parents as bitset using lattice element indexing
//
bool globallattice(const lattice* L, int k, const int* y, set ybits)
{
    int i;
    
    /* Construct upset of proposed child */
    set ubits = ybits;
    for (i=0; i<k; i++)
	ubits |= L->U[y[i]];
    
    /* Check against suspect elements. This includes all parents
       (EXCEPT those currently proposed) and all existing children. */
    for (i=L->pfirst; i<L->n; i++)
    {
	if (i < L->cfirst && ISELEMENT(&ybits, i))
	{
	    /* Do not check, it is one of the proposed parents. */
	}
	else
	{
	    /* Find all common parents of proposed child and element i. */
	    set common = ubits & L->U[i];

	    /* Find highest-numbered common parent.  Since numbering
	       starts from high bits, this corresponds to the *lowest* 1-bit in the
	       intersection. */
	    int z = ctz(common);
	    z = (WORDSIZE-1) - z; /* count from the other end! */

	    /* z should be a lower bound for all of common. That is, z's upset should cover common. */
	    if (common & ~L->U[z])
		return false;	/* Failed global lattice check */
	}
    }
    
    return true;
}


// Check the distributivity condition at proposed child.
// Any triple of its parents must not have a common parent (not even implied top).
//
// k   updegree of proposed child
// y   proposed parents (lattice element indexing)
bool distcheck(const lattice* L, int k, const int* y)
{
    int h,i,j;

    assert(k>0);
    if (k>=3 && L->V[y[0]]==0)
	return false;		/* Trivially fail if three parents are coatoms. */
    
    for (h=0; h<k; h++)
    {
	set Hh = L->H[y[h]];
	for (i=h+1; i<k; i++)
	{
	    set Hi = L->H[y[i]];
	    if ((Hh&Hi) == 0)
		continue;
	    for (j=i+1; j<k; j++)
	    {
		set Hj = L->H[y[j]];
		if ((Hh&Hi&Hj) != 0)
		{
		    /* Failed distributivity. */
		    return false;
		}
	    }
	}
    }
    return true;
}

/* Check for distributivity at bottom. If there are three or more atoms,
   since they have common child (bottom), they must not have common parent. */
bool distcheck_bottom(const lattice* L, int cfirst)
{
    int j,h,k;
    int n = L->n;

    /* Regular case. */
    for (j=cfirst; j<n; j++)
    {
	set Hj = L->H[j];
	for (h=j+1; h<n; h++)
	{
	    set Hh = L->H[h];
	    if ((Hj&Hh) == 0)
		continue;
	    for (k=h+1; k<n; k++)
	    {
		set Hk = L->H[k];
		if ((Hj&Hh&Hk) != 0)
		{
		    return false;
		}
	    }
	}
    }
    
    return true;
}

/* Check for lower semimodularity violation. Every pair of parents
   that shares a parent must share a child by now. */
bool lowersemicheck(const lattice* L)
{
    int h,i,j;
    int n      = L->n;			/* size of the mother lattice */
    int pfirst = L->pfirst;		/* first parent */
    int cfirst = L->cfirst;		/* first child */
    int k      = cfirst - pfirst;	/* number of parents */
    int lev    = L->V[pfirst];		/* level of parents */

    for (i=pfirst; i<cfirst; i++)
    {
	for (j=i+1; j<cfirst; j++)
	{
	    set Hi = L->H[i];
	    set Hj = L->H[j];
	    if (lev==0 || (Hi&Hj))
	    {
		/* They share a parent. Find common child. */
		bool found_common_child = false;
		for (h=cfirst; h<n; h++)
		{
		    if (ISELEMENT(&L->H[h], i) && ISELEMENT(&L->H[h], j))
		    {
			found_common_child = true;
			break;
		    }
		}
		if (!found_common_child)
		{
		    return false;
		}
	    }
	}
    }

    return true;
}


/****************************************************************************/
/* Auxiliary functions for iterating subsets and weak compositions. */

/* Fixed-size subsets: from n elements choose k.
   Subset is represented in two forms: x are the member indices (in 0...n-1)
   and bits are a set. */

bool subset_first(int n, int k, int* x, set* bits)
{
    if (k>n)
	return false;
    
    /* First set: Lowest k elements = 0,...,k-1 */
    int i;
    for (i=0; i<k; i++)
	x[i] = i;
    *bits = ALLMASK(k);
    
    return true;
}

bool subset_next(int n, int k, int* x, set* bits)
{
    /* Bump last element if possible. Otherwise, bump second last etc.
       If already have last k elements, then we are done. */

    int i;
    int ibump = k-1;
    while (ibump>=0 && x[ibump] >= n+ibump-k)
	ibump--;

    if (ibump < 0)
	return false;		/* Already last k elements */

    /* Bump at ibump. After that, initialize as low as possible.
       At bit level, this means that from x[ibump] all bits are cleared EXCEPT
       the k-ibump bits starting at x[ibump]+1. */
    DELELEMENT(bits, x[ibump]);
    x[ibump]++;
    ADDELEMENT(bits, x[ibump]);
    for (i=ibump+1; i<k; i++)
    {
	DELELEMENT(bits, x[i]);
	x[i] = x[i-1]+1;
	ADDELEMENT(bits, x[i]);
    }
    return true;
}


/* Weak compositions of the integer m into k distinct nonnegative parts.
   _first creates the first composition (all in first part).
   _next advances to next composition, returns false if no more solutions.
*/

bool weakcomp_first(int m, int k, int* w)
{
    int i;
    w[0] = m;
    for (i=1; i<k; i++)
	w[i] = 0;
    return true;
}

bool weakcomp_next(int m, int k, int* w)
{
    int z;

    if (k<=1)
	return false;

    /* Strategy: Find last nonzero part z (but NOT the last
       part k-1). Move 1 unit from z right to z+1.  ALSO if there is any
       stuff in the last part k-1, put that to z+1. */

    /* First take the stuff from last part */
    int stuff = w[k-1];
    w[k-1] = 0;

    /* Then scan for the last nonzero */
    z = k-2;
    while (z>=0 && w[z]==0)
    {
	z--;
    }

    /* Did we hit the left wall? */
    if (z<0)
	return false;

    /* Move some stuff right */
    w[z+1] = stuff+1;
    w[z]--;
    return true;
}

/* Partitions of the integer m into k nonnegative parts.
   A partition is represented so that the part sizes are nonincreasing.
   _first creates the first partition (all in first part).
   _next advances to next partition, returns false if no more solutions.
*/

bool partition_first(int m, int k, int* w)
{
    // First partition: All mass on the first element.
    int i;
    w[0] = m;
    for (i=1; i<k; i++)
	w[i] = 0;
    return true;
}

bool partition_next(int m, int k, int* w)
{
    int i;

    if (k<=1)
	return false;

    /* Strategy: Find rightmost part i that can be decreased.
       It must be before last part (k-1), and value >= 2.
       Also, all mass to the right, plus one from this part,
       must be redistributable to the k-i parts to the right.
    */

    int rightmass = w[k-1]+1;
    i = k-2;
    while (i>=0)
    {
	int rightlen = k-i-1;	/* Number of parts to the left */
	int rightmax = w[i]-1;	/* Each of them at most this, if w[i] is decreased */
	if (w[i]>=2 && rightlen*rightmax >= rightmass)
	{
	    /* Found it! Decrease part i by one, and redistribute all rightmass
	       as near left as possible. */
	    w[i]--;
	    int j;
	    for (j=i+1; j<=k-1; j++)
	    {
		w[j] = min(rightmass, rightmax);
		rightmass -= w[j];
	    }
	    return true;
	}

	/* Did not find yet. Move one left and accumulate rightmass. */
	rightmass += w[i];
	i--;
    }

    return false;
}

/* Hybrid partitions of the integer m.

   A hybrid partition has k distinct groups, and within i'th group, b[i] symmetric
   members.  The integer is first divided among the groups (weak composition),
   and within each group, partitioned among the members (partition).

   Parameters:
     m     integer to be hybrid-partitioned
     k     number of groups
     bb    sum of group sizes = total number of members
     b[i]  group size, for i=0,...,k-1

   Variables:
     o[i]  occupancy in i'th group
     oo[j] occupancy in j'th member; members are listed in order of groups
*/

bool hybridpartition_first(int m, int k, int bb, const int* b, int* o, int* oo)
{
    int i,j;

#if DEBUG
    // Verify that sum of group sizes is correct.
    int bbcheck = 0;
    for (i=0; i<k; i++)
	bbcheck += b[i];
    asserteq(bbcheck, bb);
#endif
    
    // First divide among groups
    if (!weakcomp_first(m, k, o))
	return false;

    int jstart=0;		/* first element of current group */
    
    // Then within each group
    for (i=0; i<k; i++)
    {
	if (!partition_first(o[i], b[i], oo+jstart))
	    return false;
	jstart += b[i];
    }

    return true;
}

bool hybridpartition_next(int m, int k, int bb, const int* b, int* o, int* oo)
{
    /* Strategy:
       Starting from the right, try to find a group whose partition can be incremented.
       If found: Reset all later groups to their initial partitions.
       If not found: Increment the weak composition and reset all groups.
    */

    int i,ii;

    // Starting positions of each group
    int jstart[k];
    jstart[0] = 0;
    for (i=0; i<k-1; i++)
	jstart[i+1] = jstart[i]+b[i];
    
    // Find group to increment.
    for (i=k-1; i>=0; i--)
    {
	if (o[i]>0)
	{
	    if (partition_next(o[i], b[i], oo+jstart[i]))
	    {
		// Reset later groups
		for (ii=i+1; ii<k; ii++)
		{
		    if (!partition_first(o[ii], b[ii], oo+jstart[ii]))
			return false;
		}
		// We are done
		return true;
	    }
	}
    }

    // Did not find group to increment.
    // Increment weak composition.
    if (!weakcomp_next(m, k, o))
	return false;
    // Reset all groups.
    for (i=0; i<k; i++)
    {
	if (!partition_first(o[i], b[i], oo+jstart[i]))
	    return false;
    }
    return true;
}





/****************************************************************************/
/* Auxiliary functions for handling lattices. */

/* Copy lattice over another. */
void copylattice(const lattice* from, lattice* to)
{
    superverify(from);
    memcpy(to, from, sizeof(lattice));
}

/* Initialize a node at index i, with no connections. Level is initialized to zero.
   Lattice size (n) is not updated. */
void latticenode(lattice* L, int i)
{
    EMPTYSET(L->H+i, MAXM);
    EMPTYSET(L->U+i, MAXM);
    ADDONEARC(L->U, i, i, MAXM);	/* self-arc, node is in its own upset */
    EMPTYSET(L->RH+i, MAXM);
    L->V[i] = 0;

    L->havecanon = false;
    L->auttype = AUNKNOWN;
}

/* Add a lattice arc. Level is updated to be at least +1 of the parent. */
void latticearc(lattice* L, int i, int j)
{
    /* Direct and reverse covering relation. */
    ADDONEARC(L->H, i, j, MAXM);
    ADDONEARC(L->RH, j, i, MAXM);
    
    /* Inherit the upset from the new parent. */
    L->U[i] |= L->U[j];

    /* Update level */
    if (L->V[i] <= L->V[j])
	L->V[i] = L->V[j]+1;

    L->havecanon = false;
    L->auttype = AUNKNOWN;
}


/****************************************************************************/
/* Functions for the main recursion: extending lattices and accepting them. */

void acceptlattice(const lattice* L)
{
    int n = L->n;
    int len;

    len = L->V[n-1] + 2;
    
    nlat_by_n[n+2]++;
    nlat_by_len[len]++;

    /* Print the lattice */
    if (onlymaxn && n < nmax)
    {
	// skip, not maximum size
    }
    else if (onlymaxlen && len < lenmax)
    {
	// skip, not maximum len
    }
    else
    {
#if DEBUG >= 2
	debugstate(L, "accept #=%lld n=%d L=", nlat_printed, L->n);
	writeg6(stderr, (graph*) L->H, MAXM, L->n);
#endif   
	if (output)
	{
	    /* Make sure it is in canonical order. */
	    const lattice *Lprint = L;
	    lattice Lcopy;
	    if (!L->havecanon)
	    {
		copylattice(L, &Lcopy);
		callnauty(&Lcopy);
		Lprint = &Lcopy;
	    }
	    assert(Lprint->havecanon);
	    superverify(Lprint);
	    
	    writed6(stdout, (graph*) Lprint->Hcanon, MAXM, Lprint->n);
	}
	nlat_printed++;
    }
}


int count_parent_pairs(const lattice* L)
{
    superverify(L);
    int i,j;
    int pfirst = L->pfirst;
    int cfirst = L->cfirst;
    
    if (L->V[pfirst] == 0)
    {
	// Special case if parents are coatoms.
	// Then they all share the implied top.
	int nparents = L->cfirst - L->pfirst;
	return numpairs(nparents);
    }

    // General case.
    
    // Find grandparents.    
    int gpfirst;		/* index of first grandparent */
    gpfirst = pfirst-1;
    while (gpfirst>0 && L->V[gpfirst-1]==L->V[pfirst]-1)
	gpfirst--;

    // Find downdegrees of grandparents.
    int gpcount = pfirst-gpfirst;
    int downdeg[gpcount];  /* number of parent-level elts covered by each gp */
    for (j=0; j<gpcount; j++)
    {
	downdeg[j] = 0;
    }
    for (i=pfirst; i<cfirst; i++)
    {
	set tmp = L->H[i];
	int gpcounter = 0;
	int gpindex = 0;
	while (tmp)
	{
	    TAKEBIT(j,tmp);
	    gpindex = j-gpfirst;
	    downdeg[gpindex]++;
	    gpcounter++;
	}
    }

    // Calculate parent-pairs from each grandparent.
    int ppneed = 0;
    for (j=0; j<gpcount; j++)
    {
	int pairs = numpairs(downdeg[j]);
	ppneed += pairs;
    }

    return ppneed;
}


/* MULTIEXTEND_SPECIAL

   Special code for modulars when updegree=2.
   Each remaining parent pair receives one child of updegree 2,
   there is no choice involved.  This might fail by
   violating the global lattice condition.
*/
void multiextend_special(lattice* L, const lattice* Lur)
{
    debugstate(L, "special\n");
    lattice Z;
    copylattice(L, &Z);

    int pfirst = L->pfirst;
    int cfirst = L->cfirst;
    int k = cfirst-pfirst;	/* number of parents */
    int i,j,g,p,q;

    if (L->V[L->pfirst]==0)
    {
	// Special case when parents are coatoms.
	// For each coatom pair, check if required child is there. If not, add it.
	for (p=pfirst; p<cfirst; p++)
	{
	    for (q=p+1; q<cfirst; q++)
	    {
		bool gotchild = (L->RH[p] & L->RH[q]) > 0;
		if (!gotchild)
		{
		    int thischild = Z.n;
		    debugstate(L, " coatoms %d,%d receive child %d\n", p, q, thischild);
		    Z.n++;
		    assert(Z.n <= nmax);
		    latticenode(&Z, thischild);
		    latticearc(&Z, thischild, p);
		    latticearc(&Z, thischild, q);
		    Z.orbit[thischild] = thischild;
		}
	    }
	}

	// Go on to decorating
	multiextend_helper(&Z, 1, false, NULL, 0, 0, Lur);
	return;
    }
    
    // Find grandparents.    
    int gpfirst;		/* index of first grandparent */
    gpfirst = pfirst-1;
    while (gpfirst>0 && L->V[gpfirst-1]==L->V[pfirst]-1)
	gpfirst--;
    int gpcount = pfirst - gpfirst;

    // Iterate over grandparents.
    for (g=gpfirst; g<pfirst; g++)
    {
	int downdeg = bitcount(L->RH[g]);
	int gpchild[downdeg];
	int downdegtmp=0;
	
	// Find children of this grandparent.
	set tmp = L->RH[g];
	while (tmp)
	{
	    TAKEBIT(p,tmp);
	    gpchild[downdegtmp] = p;
	    downdegtmp++;
	}
	asserteq(downdegtmp, downdeg);

	// For each pair, check if required child is there. If not, add it.
	for (i=0; i<downdeg; i++)
	{
	    p = gpchild[i];
	    for (j=i+1; j<downdeg; j++)
	    {
		q = gpchild[j];
		bool gotchild = (L->RH[p] & L->RH[q]) > 0;
		if (!gotchild)
		{
		    // Parents p,q need a child. Add if possible.
		    // It might fail because of the global lattice condition.
		    int y[2];
		    y[0] = p;
		    y[1] = q;
		    set ybits = 0;
		    ADDELEMENT(&ybits, p);
		    ADDELEMENT(&ybits, q);
		    if (!globallattice(&Z, 2, y, ybits))
		    {
			debugstate(&Z, "special failed, cannot add %d,%d due to global\n", p, q);
			return;
		    }

		    // Seems we can add the child.
		    int thischild = Z.n;
		    Z.n++;
		    assert(Z.n <= nmax);
		    latticenode(&Z, thischild);
		    latticearc(&Z, thischild, p);
		    latticearc(&Z, thischild, q);
		    Z.orbit[thischild] = thischild;
		}
	    }
	}
    }
    
    // Go on to decorating
    multiextend_helper(&Z, 1, false, NULL, 0, 0, Lur);
}


    
/* MULTIEXTEND_HELPER
   Extend lattice by creating child elements of specified updegree k>=2.

   L          base lattice, which may already contain children of updegree k or greater.
   k          updegree of children to create
   haveprev   if true: L already has children of degree k
   xprev      previous child (in parent indexing), only if haveprev
   bitsprev   previous child (as bitset), only if haveprev
   ppneed     parent pairs needing to be covered, only if lower semimodular
   Lur        ur-mother lattice: before any k-degree children
*/
void multiextend_helper(lattice* L, int k, bool haveprev, const int* xprev, set bitsprev,
			int ppneed, const lattice* Lur)
{
    debugstate(L, "helper n=%d pf=%d cf=%d k=%d have=%d\n", L->n, L->pfirst, L->cfirst, k, haveprev);
    
    superverify(L);
    
    bool verysimple = false;
    bool cont = false;
    int x[MAXN];
    set bits = 0;
    int i;

    int n = L->n;
    int nparent = L->cfirst - L->pfirst;   /* number of parents */
    int nchild = n - L->cfirst;   /* number of existing children */
    int lev = L->V[L->pfirst];
    int togo = nmax - n;
    bool apply_pairbudget = type_qlowersemi && (type_lowersemi || lev>0);
    int ppneed_new = ppneed;

    // Check pair budget.
    if (apply_pairbudget)
    {
	// Can we possibly cover needed parent pairs with remaining elements?
	// All remaining elements will have updegree <= k.
	int hope = togo * numpairs(k);
	if (hope < ppneed)
	    return;

	// For modulars, the pair budget works in the other direction too:
	// If we have less than k*(k-1)/2 parent pairs left, then we
	// cannot introduce any children of updegree k (or greater).
	// So maxdeg is decreased enough.
	if (type_semi && type_lowersemi && numpairs(k) > ppneed)
	{
	    while (k>1 && numpairs(k) > ppneed)
		k--;

	    // Since we decreased updegree, we no longer have same updegree.
	    haveprev = false;
	    
	    // Check pair budget again! We have decreased updegree.
	    hope = togo * numpairs(k);
	    if (hope < ppneed)
		return;
	}
    }

    if (k<=1)
    {
	// No more multiparent children -- go on to decorating.
	if (type_qlowersemi && !lowersemicheck(L))
	    return;
	int maxloners = nmax - L->n;
	debugstate(L, "decorating\n");
	if (L->auttype == AUNKNOWN)
	    callnauty(L);
	decorate(L, maxloners);
	return;
    }
    
    debugstate(L, "helper main n=%d pf=%d cf=%d k=%d have=%d\n", L->n, L->pfirst, L->cfirst, k, haveprev);

    // Consider the possibility of no k-degree children at all: Go to smaller children.
    multiextend_helper(L, k-1, false, NULL, 0, ppneed, L);

    // Remaining cases create multiparent chilren, so there must be room for at least one.
    if (L->n >= nmax)
	return;
    
    if (k==2 && type_lowersemi && type_semi)
    {
	// Special for modular lattices at updegree two.
	multiextend_special(L, Lur);
	return;
    }
    
    // Create child proposal.
    if (haveprev)
    {
	// Next proposal of same updegree as before.
	for (i=0; i<k; i++)
	    x[i] = xprev[i];
	bits = bitsprev;
	cont = subset_next(nparent, k, x, &bits);
    }
    else
    {
	cont = subset_first(nparent, k, x, &bits);
    }
    
    if (!haveprev && L->auttype != AFIXED)
    {
	lathash_push();
    }

    if (!haveprev && L->auttype == ASIMPLE)
    {
	// Check if symmetry is VERY simple, i.e. only one symbox.
	// In that case, we will consider the first proposal
	// (first k parents) but nothing else.
	verysimple = true;
	for (i=L->pfirst; i<L->cfirst; i++)
	{
	    if (L->orbit[i] != L->orbit[L->pfirst])
	    {
		verysimple = false;
		break;
	    }
	}
    }
    
    while (cont)
    {
	// Proposed child is in x and bits.
	// Before creating a lattice, see if it will work at all.

	// Convert parent indices to lattice element numbering.
	int y[MAXN];
	set ybits = 0;
	for (i=0; i<k; i++)
	{
	    y[i] = x[i] + L->pfirst;
	    ADDELEMENT(&ybits, y[i]);
	}

#if DEBUG >= 2
	debugstate(L, "propose %d-degree x=[", k);
	for (i=0; i<k; i++)
	{
	    fprintf(stderr, "%s%d", (i==0 ? "" : ""), x[i]);
	}
	fprintf(stderr, "] y=[");
	for (i=0; i<k; i++)
	{
	    fprintf(stderr, "%s%d", (i==0 ? "" : ""), y[i]);
	}
	fprintf(stderr, "]  ");
#endif
	
	if (nchild>0)
	{
	    /* Check local lattice condition at proposed child */
	    if (!locallattice(L, ybits))
	    {
		debug(stderr, "fail local\n");
		goto advance;
	    }
	}

	if (type_qsemi)
	{
	    /* Check semimodularity at proposed child. */
	    if (!semicheck(L, k, y))
	    {
		debug(stderr, "fail semi\n");
		goto advance;
	    }
	}

	if (type_dist)
	{
	    /* Check distributivity at proposed child. */
	    if (!distcheck(L, k, y))
	    {
		debug(stderr, "fail dist\n");
		goto advance;
	    }
	}

	// Check global lattice
	if (!globallattice(L, k, y, ybits))
	{
	    debug(stderr, "fail global\n");
	    goto advance;
	}

	// Update and check pair budget
	if (apply_pairbudget)
	{
	    // Modular case: All parent pairs we cover are needed.
	    ppneed_new = ppneed - numpairs(k);
	    // TODO: More careful counting when lower semimodular only (not modular).
	}

	// All good so far. Create a new lattice with the proposed child.
	lattice Z;
	copylattice(L, &Z);
	int thischild = L->n;
	Z.n = L->n + 1;
	latticenode(&Z, thischild);
	for (i=0; i<k; i++)
	    latticearc(&Z, thischild, y[i]);

	// Analyze symmetry
	if (Lur->auttype==AFIXED)
	{
	    // Easy case: The ur-mother is fixed. Adding any number
	    // of child elements cannot make it nonfixed.
	    // The new element is a singleton orbit.
	    Z.auttype = AFIXED;
	    Z.orbit[thischild] = thischild;
	}
	else if (verysimple)
	{
	    // Easy case: First child, all parents symmetric.
	    // No isomorphism checks needed.
	    // New type is also simple, with two orbits:
	    // selected parents (from pfirst to pfirst+k-1),
	    // and nonselected parents (from pfirst+k).
	    Z.auttype = ASIMPLE;
	    for (i=L->pfirst+k; i<L->cfirst; i++)
		Z.orbit[i] = L->pfirst+k;
	    Z.orbit[thischild] = thischild;
	}
	else
	{
	    // General case.
	    callnauty(&Z);
	    superverify(&Z);
	    assert(Z.havecanon);
	}
	
	// Have we seen it?
	debug(stderr, "iso?");
	if (Lur->auttype!=AFIXED && !verysimple && findiso_and_store(&Z, "helper", k))
	{
	    debug(stderr, "  isomorph\n");
	    goto advance;
	}

	// All good so far. Recurse to append more children of same updegree,
	// if possible.
	int maxdeg = k;
	
	// Optimization: If we created a child with very high updegree, then
	// updegrees of further children are severely restricted.
	// More precisely: Subsequent children can have at most the remaining nparent-k
	// parents, plus one common with this child.
	maxdeg = min(maxdeg, nparent-k+1);
	bool samedeg = (maxdeg==k);
	
	debug(stderr, "  ok\n");
	multiextend_helper(&Z, maxdeg, samedeg, x, bits, ppneed_new, Lur);
	    
    advance:
	// Advance to next proposal, if possible.
	// Exception: Not in very simple cases, because the first proposal is unique.
	if (verysimple)
	    break;
	cont = subset_next(nparent, k, x, &bits);
    }

    if (!haveprev && L->auttype != AFIXED)
    {
	lathash_pop();
    }
}


/* MULTIEXTEND
   Given a graded lattice, extend by creating multiparent children.
   seq  = sequence number within caller (for output and debug purposes only)
   L    = mother lattice
   M    = max #children to create
*/
void multiextend(long long seq, lattice* L, int M)
{
    superverify(L);
    progress(L);

    int n = L->n;
    int remelts = nmax-n;	        /* remaining max element count */
    int lev = L->V[L->pfirst];		/* level of parents */
    int nparent = n - L->pfirst;	/* number of parents */
    int ppneed = 0;			/* number of parent pairs not yet covered */
    long long mprop = 0;
    long long nlp = nlat_printed;

#if DEBUG >= 2
    debugstate(L, "multiextend n=%d pf=%d cf=%d plev=%d=%d\n  L=", L->n, L->pfirst, L->cfirst, L->V[L->pfirst], L->V[n-1]);
    writeg6(stderr, (graph*) L->H, MAXM, L->n);
#endif
    asserteq(L->cfirst, L->n);	        /* no children at this point */
    asserteq(L->V[n-1], lev);           /* all parents on same level */

    if (branchfile && n >= branchat)
    {
	writed6(branchfile, (graph*) L->H, MAXM, L->n);
	branchoutputcount++;
	return;
    }
    
    /* time class 20,40,60 depending on mother type AND parent count (at most 19) */
    switch_timeclass(L->auttype*20 + min(nparent,19), 1);

    if (type_qlowersemi && (type_lowersemi || lev>0))
	ppneed = count_parent_pairs(L);

    int maxdeg = nparent;
    if (lev==0 && type_dist)
	maxdeg = min(maxdeg, 2);
    
    multiextend_helper(L, maxdeg, false, NULL, 0, ppneed, L);
}


bool is_permutation(int k, const int* perm)
{
    bool seen[k];
    int h,i,j;
    for (i=0; i<k; i++)
	seen[i] = false;
    for (i=0; i<k; i++)
    {
	j = perm[i];
	if (j<0 || j>=k || seen[j])
	{
	    fprintf(stderr, "OOPS. Not a %d-permutation:", k);
	    for (h=0; h<k; h++)
		fprintf(stderr, " %d", perm[h]);
	    fprintf(stderr, "\n");
	    return false;
	}
	seen[j] = true;

	if (i!=j)
	{
	    fprintf(stderr, "\n\n\n*********************");
	    fprintf(stderr, "HELLO. Nonidentity permutation ");
	    for (h=0; h<k; h++)
		fprintf(stderr, " %d", perm[h]);
	    fprintf(stderr, "\n*********************\n\n\n");
	    die("now");
	}
    }
    return true;
}

/* DECORATE
   Given a graded lattice, extend by creating singleparent children.
   seq  = sequence number within caller (for output and debug purposes only)
   L    = mother lattice
   Mmax = max #children to create
*/
void decorate(const lattice* L, int Mmax)
{
    superverify(L);
    progress(L);

    struct timespec decofrom;
    double decoelapsed = 0.0;
    starttimer(&decofrom, &decoelapsed);
    
    int M,h,i,j;
    bool more;
    set havechild;

    int n      = L->n;			/* size of the mother lattice */
    int pfirst = L->pfirst;		/* first parent */
    int cfirst = L->cfirst;		/* first child */
    int k      = cfirst - pfirst;	/* number of parents */
    int lev    = L->V[pfirst];		/* level of parents */

    if (type_dist && minneck>=2)
    {
	/* Compute local upper bound for lattice length.
	   Parents are at level lev, children at lev+1, this would make length = lev+3.
	   Assume we use at least 1 elt here for loners (otherwise upper bound is zero!).
	   With at least 2 elts, we can form a new atom level.
	   With what remains, we can form remains/minneck more levels.
	*/
	int already_len = lev+3;
	int remelt = nmax - L->n;
	if (remelt>=2)
	{
	    already_len += 1;
	    remelt -= 2;
	}
	int remloners = already_len + remelt/minneck - L->loners;

	/* Now if we spend remloners elements, this may cause the length to decrease,
	   so we may have to decrease remloners even further. */
	while (remloners>0)
	{
	    if (L->loners + remloners > already_len + (remelt-remloners)/minneck)
		remloners--;
	    else
		break;
	}

	if (Mmax > remloners)
	    Mmax = remloners;
    }
    
    /* time class 80,100,120 depending on mother type, AND parent count (at most 19) */
    int theclass = 60 + L->auttype*20 + min(k,19);
    double beforethis = stat_timeclass_elapsed[theclass];
    switch_timeclass(theclass, 1);

    assert(pfirst <= cfirst);
    assert(cfirst <= n);
    asserteq(L->V[cfirst-1], lev);
    if (cfirst<n && (L->V[cfirst] != lev+1 || L->V[n-1] != lev+1))
	die("oops, child level mismatch");

#if DEBUG >= 2
    debugstate(L, "decorate n=%d Mmax=%d lev=%d par=%d:%d child=%d:%d L=",
	    n, Mmax, lev, pfirst, cfirst-1, cfirst, n-1);
    writeg6(stderr, (graph*) L->H, MAXM, L->n);
#endif

    /* In coatomic lattices, we do not allow single-parent children */
    if (type_coatomic)
	Mmax = 0;
    
    /* Find which parents already have children */
    bool E[k];
    int Esum = 0;
    havechild = 0;
    for (i=pfirst+k; i < n; i++)
	havechild |= L->H[i];
    for (i=0; i<k; i++)
    {
	E[i] = !ISELEMENT(&havechild, pfirst+i);
	Esum += E[i];
    }

    if (Esum>Mmax)
    {
	/* No solutions since not enough children. */
	return;
    }

    /* Count off the required children from the movable ones. */
    Mmax -= Esum;

    /* If vertical indecomposability is required, then total child
       count must be at least 2. */
    int Mmin = 0;
    if (type_indeco)
    {
	/* Total child count will be:
	   n-cfirst multiparent children (already placed)
	   M+Esum singleparent children (will be placed).
	   Since we require
	     n-cfirst+Esum+M >= 2,
	   equivalently
	     M >= 2 - Esum - (n-cfirst).
	*/
	Mmin = max(0, 2 - Esum - (n-cfirst));
    }

    /* We are going to use the stack only if the mother is complex. */
    if (L->auttype==AOTHER)
	lathash_push();

    /* These will be used only if ASIMPLE. */
    int norbit = 0;
    int orbitsize[k];		/* size of each orbit, really only up to norbit-1 */
    int orbitof[k];		/* from 0-based parent index to 0-based orbit index */
    int orbitocc[k];
    int memberocc[k];
    int membermap[k];
    
    if (L->auttype==ASIMPLE)
    {
	/* Find and count orbits. */
	
	for (i=0; i<k; i++)
	    orbitsize[i] = 0;
	
	for (i=pfirst; i<cfirst; i++)
	{
	    int iorb;		/* 0-based orbit index */
	    if (L->orbit[i]==i)
	    {
		/* Begins new orbit */
		iorb = norbit;
		orbitsize[iorb] = 1;
		norbit++;
	    }
	    else
	    {
		/* Same orbit as element L->orbit[i] */
		iorb = orbitof[L->orbit[i] - pfirst];
		assert(iorb < norbit);
		orbitsize[iorb]++;
	    }
	    orbitof[i-pfirst] = iorb;
	}
    }
    
    bool verysimple = (L->auttype==ASIMPLE && norbit==1);
    bool usehybrid  = (L->auttype==ASIMPLE && norbit>1);

    if (usehybrid)
    {
	// Prepare to use hybrid partitions.
	// Create a map between element indexes, from memberocc to w.
	// memberocc has occupancied grouped by orbit.
	// w needs them in parent indexing.
	
	int orbitstart[k];
	orbitstart[0] = 0;
	for (i=1; i<norbit; i++)
	    orbitstart[i] = orbitstart[i-1] + orbitsize[i-1];

	int orbitnext[k];
	for (i=0; i<norbit; i++)
	    orbitnext[i] = orbitstart[i];

	for (i=0; i<k; i++)
	{
	    int iorb = orbitof[i];
	    membermap[orbitnext[iorb]] = i;
	    orbitnext[iorb]++;
	}
	
#if DEBUG
	assert(is_permutation(k, membermap));
#endif
    }
    
    long long mseq = 0;
    long long dprop = 0;

    for (M=Mmax; M>=Mmin; M--)
    {
	/* Place M movable children (and the Esum required children). */
	int totalchildren = M + Esum;

	int w[k];
	
	if (verysimple)
	    more = partition_first(M,k,w);
	else if (usehybrid)
	    more = hybridpartition_first(M, norbit, k, orbitsize, orbitocc, memberocc);
	else
	    more = weakcomp_first(M,k,w);
       
	while (more)
	{
	    bool accept  = true;
	    lattice Z;		/* decorated lattice */
	    bool noextend = false;
	    int par   = pfirst;	          /* index of current parent */
	    int child = n;		  /* index of current child: start after all old elements */
	    bool any_multichild = false;  /* flag: did any parent get 2 or more children */
	    int wplus[k];		  /* number of children added to each parent */

	    if (usehybrid)
	    {
		// Reorder occupancies from memberocc to w, using the map.
		for (i=0; i<k; i++)
		    w[membermap[i]] = memberocc[i];
	    }
	    
	    dprop++;
	    
	    for (i=0; i<k; i++)
	    {
		int addnow = w[i] + E[i]; /* number of children to add now: movable + required */
		wplus[i] = addnow;
		
		if (addnow >= 2)
		    any_multichild = true;
	    }

	    /* Start by copying the mother lattice */
	    copylattice(L, &Z);
	    Z.n += totalchildren;
	    Z.havecanon = false;
	    
	    /* Finish the lattice by adding the children */
	    int first_sibling[MAXN];

	    for (i=0; i<k; i++)
	    {
		int addnow = wplus[i];
		set hbar = 0;
		ADDELEMENT(&hbar, par);   /* same cover for all children */

		int fc = child;
		
		for (j=0; j<addnow; j++)
		{
		    set ubar = L->U[par];
		    ADDELEMENT(&ubar, child); /* upset for this child */

		    Z.H[child] = hbar;
		    Z.U[child] = ubar;
		    Z.V[child] = lev+1;
		    Z.RH[child] = 0;
		    ADDELEMENT(&Z.RH[par], child);
		    
		    Z.orbit[child] = n; /* may be refined later */
		    Z.loners++;
		    
		    first_sibling[child-n] = fc;
		    child++;
		}
		par++;
	    }

	    /* Advance levels. Child level becomes new parent level, and
	       new child level is empty. */
	    Z.pfirst = Z.cfirst;
	    Z.cfirst = Z.n;
	    int width_completed = Z.n - Z.pfirst;
	    
	    if (L->auttype==AFIXED)
	    {
		// Mother lattice is all fixed (at parent and child levels).
		// Then all daughter lattices are nonisomorphic.
		//
		// Also, their automorphism groups will be fixed or simple.
		// We need not call nauty.
		// But we have to fill in the orbit: if some parent gained
		// multiple children, then those children are symmetric.
		if (any_multichild)
		    Z.auttype = ASIMPLE;
		else
		    Z.auttype = AFIXED;
		for (i=n; i<Z.n; i++)
		    Z.orbit[i] = first_sibling[i-n];
	    }
	    else if (L->auttype==ASIMPLE || L->auttype==AOTHER)
	    {
		// Mother lattice is not fixed. Resort to full nauty for symmetry analysis.
		// But isomorph checking needed only in the other case.
		superverify(&Z);
		callnauty(&Z);

		if (L->auttype==AOTHER && findiso_and_store(&Z, "decorate", k))
		{
		    /* Isomorph found. Do nothing. */
		    accept = false;
		}
	    }
	    else
	    {
		die("unexpected type %d", L->auttype);
	    }

	    /* Accept to lattice list */
	    if (accept)
	    {
		bool bottomok = true;
		if (type_semi && !semicheck_bottom(&Z, pfirst+k))
		    bottomok = false;
		if (bottomok && type_dist && !distcheck_bottom(&Z, pfirst+k))
		    bottomok = false;

		if (bottomok)
		{
		    acceptlattice(&Z);
		}

		/* Decide if we can continue recursion to a new level of elements. */
		bool dorecurse = true;

		/* Are we at maximum length already? */
		if (lev >= lenmax-3)
		    dorecurse = false;

		// How many elements can we still place?
		// Next level needs at least 1 element, or 2 elements if requiring indecomposable.
		int nroom = nmax - Z.n;
		if (nroom <= 0)
		    dorecurse = false;

		if (nroom <= 1 && type_indeco)
		    dorecurse = false;

		// If current level is smaller than neck width, do not continue recursion.
		if (width_completed < minneck)		    
		    dorecurse = false;

		if (dorecurse)
		{
		    if (noextend)
			die("oops");
		    stoptimer(&decofrom, &decoelapsed);
		    multiextend(mseq, &Z, nroom);
		    starttimer(&decofrom, &decoelapsed);
		    switch_timeclass(60 + L->auttype*20 + min(k,19), 0); /* after multiextend, switch BACK to our decorate class */
		    mseq++;
		}
	    }
		
	    /* Advance */
	nextweak:
	    if (verysimple)
		more = partition_next(M,k,w);
	    else if (usehybrid)
		more = hybridpartition_next(M, norbit, k, orbitsize, orbitocc, memberocc);
	    else
		more = weakcomp_next(M,k,w);
	}

    }

    stoptimer(&decofrom, &decoelapsed);
    
    /* We used the stack only if the mother is complex. */
    if (L->auttype==AOTHER)
	lathash_pop();
}



/* Create a diamond lattice with n coatoms.  Since top and bottom are
   stripped, this is simply n uncomparable elements. */
void makediamond(int n, lattice* L)
{
    int i;
    
    memset(L, 0, sizeof(lattice));
    L->n = n;
    L->loners = n;
    
    for (i=0; i<n; i++)
    {
	latticenode(L, i);
	L->V[i] = 0;
	L->orbit[i] = 0;
    }

    L->pfirst = 0;		/* We continue with coatoms as parents. */
    L->cfirst = n;		/* No children yet. */

    L->auttype = ASIMPLE;	/* Symmetric group. */
    memcpy(L->Hcanon, L->H, sizeof(L->Hcanon));
    L->havecanon = true;
}

/* Root of the recursion: Start from a diamond of n coatoms (and
   implicit top and bottom). */
void root(int n)
{
    lattice L;
    int i;

    if (type_dist && n>=3)
    {
	/* Special coatom limit in distributive lattices.
	   Because all coatoms are children of top, every pair of coatoms
	   requires a different connector of updegree 2, that is
	   n(n-1)/2 connectors if we have n coatoms.
	*/
	if (n + n*(n-1)/2 > nmax)
	    return;
    }

    makediamond(n, &L);
    current_root = n;
    
    if (n<=2 || !type_dist)
	acceptlattice(&L);
    
    if (lenmax >= 3)
	multiextend(n, &L, nmax-n);
}


/****************************************************************************/
/* Main program and helpers. */

void help()
{
    fprintf(stderr,
	    "gralist: List graded lattices.\n"
	    "\n"
	    "gralist [options] maxn [maxlen] [mincoat] [maxcoat]\n"
	    "\n"
	    "Lattice output options:\n"
	    "  -o     Output lattices to stdout in digraph6 format\n"
	    "  -m     Only output lattices of maximum size\n"
	    "  -M     Only output lattices of maximum length\n"
	    "\n"
	    "Lattice type options:\n"
	    "  -s     Semimodular lattices\n"
	    "  -S     Semimodular except possibly at atoms\n"
	    "  -l     Lower semimodular lattices (-sl for modular)\n"
	    "  -L     Lower semimodular except possibly at coatoms\n"
	    "  -i     Vertically indecomposable lattices\n"
	    "  -c     Coatomic lattices (-lc for co-geometric)\n"
	    "  -d     Distributive lattices (implies -sl)\n"
	    "  -nX    Minimum neck width (levels between coatoms and atoms)\n"
	    "\n"
	    "Other options:\n"
	    "  -bN    Break into branches after N elements\n"
	    "  -jK/M  Run Kth job out of M total (approx K/M of work)\n"
	    "  -t     Show statistics at end\n"
	    "  -PX    Show progress every X seconds, default=%d\n",
	    progress_every
	);
    exit(1);
}

void die(const char* format, ...)
{
    va_list arglist;

    fprintf(stderr, "gralist: ");
    va_start(arglist, format);
    vfprintf(stderr, format, arglist);
    va_end(arglist);
    fprintf(stderr, "\n");

#if DEBUG
    int z = 1/0;		/* This triggers a break in gdb. */
#endif
    
    exit(1);
}

void runwithlattice(const graph* g, int n)
{
    int i, j;
    lattice L;
    
    memset(&L, 0, sizeof(lattice));
    L.n = n;
    L.loners = 0;
    
    /* Fill in the graphs (H,U) and levels (V). */
    for (i=0; i<n; i++)
    {
	latticenode(&L, i);
	setword gi = g[i];
	int updegree = 0;
	while (gi)
	{
	    TAKEBIT(j, gi);
	    
	    /* Process arc (i,j). */
	    if (j>=i || j>n)
		die("invalid arc (%d,%d)\n", i, j);

	    latticearc(&L, i, j);
	    updegree++;
	}

	// Count as loner if updegree=1 or =0 (latter is really =1, coatoms with implied parent).
	if (updegree <= 1)
	    L.loners++;
    }

    /* Set parent and child levels. Parent level is the last existing level. Child level is empty. */
    int Vmax = L.V[n-1];
    for (i=0; i<n; i++)
    {
	if (L.V[i]==Vmax)
	{
	    L.pfirst = i;
	    break;
	}
    }
    L.cfirst = n;

    /* Analyze symmetry and run */
    callnauty(&L);
    multiextend(n, &L, nmax-n);
}

void runjob()
{
    int m = 1;
    int n = 0;
    int lineno = 0;
    int branchno = 0;
    graph g[MAXN];
    boolean digraph;
    FILE* f = fopen(branchfilename, "r");
    
    if (!f)
	die("cannot read branch file \"%s\"", branchfilename);

    while (readgg(f, g, 1, &m, &n, &digraph))
    {
	lineno++;
	if (!digraph)
	    die("line %d not digraph\n", lineno);

	int jobsel = lineno % jobcount;
	if (jobsel==jobid)
	{
	    branchno++;
	    lattice L;

	    current_branch = branchno;
	    current_line   = lineno;
	    
	    runwithlattice(g, n);
	}
    }
    fprintf(stderr, "did lines=%d, branches=%d\n", lineno, branchno);

    fclose(f);
}

void runroot()
{
    int n;
    if (nmax>=1 && lenmax>=2)
    {
	for (n=coatmin; n<=coatmax; n++)
	{
	    root(n);
	}
    }
}

int main(int argc, char** argv)
{
    int n, i, j;

    if (argc < 2)
	help();

    while (argc >= 2 && argv[1][0]=='-')
    {
	for (i=1; argv[1][i]; i++)
	{
	    switch (argv[1][i])
	    {
	    case 'o': output = true; break;
	    case 'm': onlymaxn = true; break;
	    case 'M': onlymaxlen = true; break;
	    case 's': type_semi = true; type_qsemi = true; break;
	    case 'l': type_lowersemi = true; type_qlowersemi = true; break;
	    case 'S': type_qsemi = true; break;
	    case 'L': type_qlowersemi = true; break;
	    case 'i': type_indeco = true; break;
	    case 'c': type_coatomic = true; break;
	    case 'd': type_dist = true; break;
	    case 't': showstats = true; break;
	    case 'w': /* w=n for compatibility */
	    case 'n': minneck = atoi(&argv[1][i+1]); goto nextarg;
	    case 'a': /* a=b for compatibility */
	    case 'b': branchat = atoi(&argv[1][i+1]); goto nextarg;
	    case 'j': sscanf(&argv[1][i+1], "%d/%d", &jobid, &jobcount); jobs=true; goto nextarg;
	    case '6': /* ignore for backward compatibility */ break;
	    case 'P': progress_every = atoi(&argv[1][i+1]); if (progress_every<=0) die("progress every <= 0"); goto nextarg;
	    case 'h': help(); break;
	    default: die("unknown option letter %c", argv[1][i]); break;
	    }
	}
    nextarg:
	argc--;
	argv++;
    }

    if (argc < 2)
	help();

    nmax  = atoi(argv[1]);

    sprintf(branchfilename, "branch%d", nmax);
    if (jobid<0 || jobid>=jobcount)
	die("invalid jobid %d/%d", jobid, jobcount);
    if (branchat > 0)
    {
	branchfile = fopen(branchfilename, "w");
	if (!branchfile)
	    die("cannot create branch file \"%s\"", branchfilename);
    }
    
    if (type_indeco && minneck<2)
	minneck = 2;
    
    if (type_dist)
	maxloners = nmax/minneck + 1;
    
    if (argc >= 3)
	lenmax = atoi(argv[2]);
    else
	lenmax = nmax-1;
    if (argc >= 4)
	coatmin = atoi(argv[3]);
    if (argc >= 5)
	coatmax = atoi(argv[4]);

    /* Internally we count n without top and bottom. */
    nmax -= 2;

    if (nmax<1 || nmax>=MAXN)
	die("invalid nmax");
    
    if (type_coatomic)
	type_indeco = true;	/* Coatomic implies indecomposable. */

    if (type_indeco && coatmin<2)
	coatmin = 2;

    if (type_dist)
    {
	type_semi = true;
	type_qsemi = true;
	type_lowersemi = true;
	type_qlowersemi = true;
    }
    
    if (coatmax > nmax)
	coatmax = nmax;

    /* Print starting banner. */
    fprintf(stderr, "gralist maxn=%d maxlen=%d mincoat=%d maxcoat=%d", nmax+2, lenmax, coatmin, coatmax);
    if (type_semi && type_lowersemi)
    {
	fprintf(stderr, " modular");
    }
    else if (type_qsemi && type_qlowersemi)
    {
	fprintf(stderr, " quasimodular");
    }
    else
    {
	if (type_semi)
	    fprintf(stderr, " semimodular");
	else if (type_qsemi)
	    fprintf(stderr, " quasisemimodular");
	if (type_lowersemi)
	    fprintf(stderr, " lower-semimodular");
	else if (type_qlowersemi)
	    fprintf(stderr, " lower-quasisemimodular");
    }
    if (type_indeco)
	fprintf(stderr, " indecomposable");
    if (type_coatomic)
	fprintf(stderr, " coatomic");
    if (minneck > 0)
        fprintf(stderr, " neck=%d", minneck);
    if (branchat > 0)
	fprintf(stderr, " branchat=%d", branchat);
    if (jobs)
	fprintf(stderr, " job=%d/%d", jobid, jobcount);
    fprintf(stderr, "\n");

    fprintf(stderr, "Compiled %s %s\n", __DATE__, __TIME__);
    fprintf(stderr, "\n");

    printclock();
    fprintf(stderr, " started\n----\n");
    
    initnauty();
    init_timeclass();
    init_progress();
    
    /* Create the base graphs and recurse. */
    if (jobs)
	runjob();
    else
	runroot();

    fprintf(stderr, "----\n");
    printclock();
    fprintf(stderr, " finished, fwall=%d, fcpu=%.2f\n\n", wallspent(), cpuspent());

    if (branchfile)
    {
	fprintf(stderr, "wrote %d branches\n\n", branchoutputcount);
	fclose(branchfile);
    }
    
    if (showstats)
    {
	fprintf(stderr,"Statistics from nauty\n");
	long long total_calls = 0;
	double total_time = 0;
	for (j=0; j<=STAT_NAUTY_MAXGROUP; j++)
	{
	    if (stat_nauty_calls[j] > 0)
	    {
		fprintf(stderr, "nauty %2d calls %9lld, time %10.3f s, average %10.3f us\n",
			j, stat_nauty_calls[j], stat_nauty_time[j],
			stat_nauty_time[j]/stat_nauty_calls[j]*1e6);
		total_calls += stat_nauty_calls[j];
		total_time  += stat_nauty_time[j];
	    }
	}
	fprintf(stderr, "nauty ** calls %9lld, time %10.3f s, average %10.3f us\n",
		total_calls, total_time,
		total_time/total_calls*1e6);
	fprintf(stderr,"\n");

	finish_timeclass();
	fprintf(stderr, "timeclass stats\n");
	double timeclass_total = 0.0;
	for (j=0; j<=STAT_TIMECLASS_MAX; j++)
	{
	    timeclass_total += stat_timeclass_elapsed[j];
	}
	for (j=0; j<=STAT_TIMECLASS_MAX; j++)
	{
	    fprintf(stderr, "class %3d: time=%12.6f s (perc=%5.2f) (count=%lld)\n",
		    j, stat_timeclass_elapsed[j], 100.0*stat_timeclass_elapsed[j]/timeclass_total, stat_timeclass_count[j]);
	}
	fprintf(stderr,"\n");
	
	fprintf(stderr,"Statistics from hash\n");
	for (j=0; j<3; j++)
	    fprintf(stderr,"%12lld mismatch by hash[%d]\n", hashmismatch[j], j);
	fprintf(stderr,"%12lld mismatch by edges\n", hashmismatchfull);
	lathashtotals();    
	fprintf(stderr,"\n");

    }

    /* Print counts by lattice size. */
    fprintf(stderr, "Counts by lattice size\n");
    for (i=0; i<=nmax+2; i++)
    {
	if (nlat_by_n[i])
	    fprintf(stderr, "BYN\t%2d\t%10lld\n", i, nlat_by_n[i]);
    }
    fprintf(stderr,"\n");
    
    /* Print counts by lattice length. */
    fprintf(stderr, "Counts by lattice length\n");
    for (i=0; i<=lenmax; i++)
    {
	if (nlat_by_len[i])
	    fprintf(stderr, "BYLEN\t%2d\t%10lld\n", i, nlat_by_len[i]);
    }
    fprintf(stderr,"\n");

    return 0;
}
