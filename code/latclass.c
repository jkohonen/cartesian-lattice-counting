// latclass -- Statistics of lattice class
//
// Lattices are read from file (or stdin).
// Format is "digraph6".
// Lattices must be graded, top and bottom stripped off, and in level order (starting from coatoms).


#define MAXN 60

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>

#include "nauty.h"
#include "nautinv.h"
#include "gtools.h"

// Nauty auxiliaries
static void userautomproc(int count, int *p, int *orbits, int numorbits,
			  int stabvertex, int n);
static DEFAULTOPTIONS_DIGRAPH(options);
static statsblk stats;

// Workpad for storing generators.
// Has to be global as userautomproc needs to access it.
static int Gtmp[MAXN*MAXN];
static int Glen;

// Lattice classes
typedef enum
{
    unknowntype,
    sp,				/* specials: short or >=3 elts both ends */
    mf, ma, mc, mx, mh,		/* middle pieces, five subtypes */
    bf, bs,			/* bottom pieces, fixed or symm coatoms */
    tf, ts,			/* top pieces, fixed or symm atoms */
    cf, cs, cn,			/* compositions, fixed, symm or too-many coatoms */
    lattypelen,			/* pseudo for number of types */
} lattype;
const lattype lattypefirst = sp;
const lattype lattypelast  = cn;

const char* lattypes[] = {
    "unknowntype",
    "sp",
    "mf", "ma", "mc", "mx", "mh",
    "bf", "bs",
    "tf", "ts",
    "cf", "cs", "cn",
    "oops"
};
    
// Statistics
long long lineno = -1;
long long latcount[lattypelen];
clock_t start, finish;

// Flags and arguments
int nseen = -1;			/* lattice size; all lattices must have the same size */
bool mode_list;
bool mode_signature;


// Auxiliary functions

void printclock()
{
    struct timeval tv;
    time_t nowtime;
    struct tm *nowtm;
    char buf[64];

    gettimeofday(&tv, NULL);
    nowtime = tv.tv_sec;
    nowtm = localtime(&nowtime);
    strftime(buf, sizeof buf, "%Y-%m-%d %H:%M:%S", nowtm);
    fprintf(stderr, "%s", buf);
}

int max(int a, int b)
{
    return (a>b) ? a : b;
}

int min(int a, int b)
{
    return (a<b) ? a : b;
}

void die(const char* format, ...)
{
    va_list arglist;

    fprintf(stderr, "latclass: ");
    if (lineno >= 0)
	fprintf(stderr, "line=%lld: ", lineno);
    va_start(arglist, format);
    vfprintf(stderr, format, arglist);
    va_end(arglist);
    fprintf(stderr, "\n");

#if DEBUG
    int z = 1/0;		/* This triggers a break in gdb. */
#endif
    
    exit(1);
}

void initnauty()
{
    /* verify we have compatible versions of the nauty routines. */
    nauty_check(WORDSIZE,MAXM,MAXN,NAUTYVERSIONID);

    // Change some options
    options.getcanon      = FALSE;
    options.digraph       = TRUE;
    options.defaultptn    = FALSE;
    options.userautomproc = userautomproc;
    options.invarproc     = adjacencies;
    options.invararg      = 0;		/* default is 0 */
}

/* This is called by nauty for each generator of the automorphism group. */
static void userautomproc(int count, int *p, int *orbits, int numorbits,
			  int stabvertex, int n)
{
    if (count != Glen+1 || Glen > n)
	die("internal error: n=%d Glen=%d count=%d", n, Glen, count);
    memcpy(Gtmp+Glen*n, p, n*sizeof(int));
    Glen++;
}

/* Call nauty, obtaining orbits, canonical labeling and generators. */
void callnauty(int n, const graph* g, const int* V)
{
    int lab[MAXN];
    int ptn[MAXN];
    int orbit[MAXN];
    int i;

    /* Initial colors by level */
    for (i=0; i<n; i++)
    {
	lab[i] = i;
	ptn[i] = (i<n-1 && V[i]==V[i+1]);
	/* ptn[i] is true if this element does NOT end a color class,
	   that is, it is not last and it has same level as next elem. */
    }
    
    Glen = 0;			/* Empty list of generators */
    
    /* Call it! We trust it does not change the first parameter. */
    densenauty((graph*) g, lab, ptn, orbit, &options, &stats, MAXM, n, NULL);

}    


lattype topclass(int n, int minatom, int maxatom)
{
    int i,j;
    int natom = maxatom-minatom+1;
    
    if (natom != 2)
	die("topclass unexpected atom count %d", natom);

    if (Glen==0)
	return tf;

    bool anymove = false;
    for (i=0; i<Glen; i++)
    {
	/* How many elements does this generator move? 
           We only care about elements that are atoms.
	   It should either move 2 atoms or zero (if it moves something
	   else). */
	int nmove = 0;
	for (j=minatom; j<=maxatom; j++)
	{
	    if (Gtmp[i*n + j] != j)
		nmove++;
	}
	if (nmove == 2)
	    return ts;
	if (nmove != 0)
	    die("oops! unexpected generator in atomsymm");
    }

    return tf;
}

lattype bottomclass(int n, int mincoat, int maxcoat)
{
    int i,j;
    int ncoat = maxcoat-mincoat+1;

    if (ncoat != 2)
	die("bottomclass uneexpected coatom count %d", ncoat);
    
    if (Glen==0)
	return bf;

    bool anymove = false;
    for (i=0; i<Glen; i++)
    {
	/* How many elements does this generator move? 
           We only care about elements that are coatoms. */
	int nmove = 0;
	for (j=mincoat; j<=maxcoat; j++)
	{
	    if (Gtmp[i*n + j] != j)
		nmove++;
	}
	if (nmove == 2)
	    return bs;
	if (nmove != 0)
	    die("oops! unexpected generator in coatsymm");
    }

    return bf;
}


lattype middleclass(int n, int minatom, int maxatom, int mincoat, int maxcoat, const graph* d)
{
    int i,j;
    int natom = maxatom-minatom+1;
    int ncoat = maxcoat-mincoat+1;
    
    if (natom != 2)
	die("middleclass unexpected atom count %d", natom);
    
    if (ncoat != 2)
	die("middleclass unexpected coatom count %d", ncoat);
    
    if (Glen==0)
	return mf;

    int gencount_twoatoms = 0;
    int gencount_twocoats = 0;
    int gencount_twocoats_twoatoms = 0;
    
    bool anymove = false;
    for (i=0; i<Glen; i++)
    {
	/* How many elements does this generator move? 
           We only care about elements that are coatoms OR atoms. */
	int nmovecoat = 0;
	int nmoveatom = 0;
	for (j=mincoat; j<=maxcoat; j++)
	{
	    if (Gtmp[i*n + j] != j)
		nmovecoat++;
	}
	for (j=minatom; j<=maxatom; j++)
	{
	    if (Gtmp[i*n + j] != j)
		nmoveatom++;
	}
	if (nmovecoat==2 && nmoveatom==0)
	    gencount_twocoats++;
	else if (nmovecoat==0 && nmoveatom==2)
	    gencount_twoatoms++;
	else if (nmovecoat==2 && nmoveatom==2)
	    gencount_twocoats_twoatoms++;
    }

    lattype typ;

    if (gencount_twocoats==0 && gencount_twoatoms==0 && gencount_twocoats_twoatoms==0)
	typ = mf;
    else if ((gencount_twocoats>0) + (gencount_twoatoms>0) + (gencount_twocoats_twoatoms>0) >= 2)
	typ = mx;
    else if (gencount_twocoats > 0)
	typ = mc;
    else if (gencount_twoatoms > 0)
	typ = ma;
    else if (gencount_twocoats_twoatoms > 0)
	typ = mh;
    else
	die("oops! classification logic fails in middleclass %d,%d,%d",
	    gencount_twocoats, gencount_twoatoms, gencount_twocoats_twoatoms);
    
    return typ;
}


void process(int n, const graph* g)
{
    int V[n], Vmax;
    int Vstart[n+1], Vlen[n+1];
    graph d[MAXN];
    int i,j,k,pa,v,waist;
    int natom,ncoat;
    lattype typ = unknowntype;
    
    initnauty();

    EMPTYGRAPH(d, 1, n);
    memset(V, 0, n*sizeof(int));
    memset(Vstart, 0, (n+1)*sizeof(int));
    memset(Vlen, 0, (n+1)*sizeof(int));
    
    // Find levels. Check that lattice is graded and in level order.
    Vmax = 0;
    for (i=0; i<n; i++)
    {
	if (g[i]==0)
	{
	    V[i] = 0;
	}
	else
	{
	    set h = g[i];
	    bool first = true;
	    while (h)
	    {
		TAKEBIT(pa,h);
		
		if (pa >= i)
		    die("not in order\n");
		if (!first && V[i] != V[pa]+1)
		    die("not graded\n");

		V[i] = V[pa]+1;
		first = false;
		ADDONEARC(d, pa, i, 1);
	    }
	    Vmax = max(Vmax, V[i]);
	}
    }

    // Find level sizes and verify increasing order
    for (i=0; i<n; i++)
    {
	if (i>0 && V[i]<V[i-1])
	    die("not in order\n");

	if (i==0 || V[i]>V[i-1])
	    Vstart[V[i]] = i;

	if (i==n-1 || V[i]<V[i+1])
	    Vlen[V[i]] = i - Vstart[V[i]] + 1;
    }
    ncoat = Vlen[0];
    natom = Vlen[Vmax];

    // Safeguard.
    Vstart[Vmax+1] = n;
    Vlen[Vmax+1] = 0;

    // Find smallest waist (below coats and above atoms)
    waist = 999;
    for (v=1; v<=Vmax-1; v++)
    {
	if (Vlen[v] <= waist)
	    waist = Vlen[v];
    }

    // Call nauty to analyze symmetry
    callnauty(n, g, V);

    // Classify
    if (Vmax<=0)
    {
	// Special: too short
	typ = sp;
    }
    else if (waist==2)
    {
	// Composition.
	if (ncoat >= 3)
	{
	    typ = cn;
	}
	else
	{
	    // First find the coatom symmetry as if bottom piece.
	    lattype symmetry = bottomclass(n, Vstart[0], Vstart[0]+Vlen[0]-1);
	    // Then convert to corresponding composition type.
	    typ = symmetry - bf + cf;
	}
    }
    else if (natom>2 && ncoat>2)
    {
	// Special: too many coats and atoms
	typ = sp;
    }
    else if (natom>2 && ncoat==2)
    {
	// Bottom piece
	typ = bottomclass(n, Vstart[0], Vstart[0]+Vlen[0]-1);
    }
    else if (natom==2 && ncoat>2)
    {
	// Top piece
	typ = topclass(n, Vstart[Vmax], Vstart[Vmax]+Vlen[Vmax]-1);
    }
    else if (natom==2 && ncoat==2)
    {
	// Middle piece
	typ = middleclass(n, Vstart[Vmax], Vstart[Vmax]+Vlen[Vmax]-1,
			  Vstart[0], Vstart[0]+Vlen[0]-1, d);
    }
    else
    {
	die("did not classify");
    }

    latcount[typ]++;

    if (mode_list)
    {
	printf("lat\t%s", lattypes[typ]);
	if (mode_signature)
	{
	    printf("\t");
	    for (v=0; v<=Vmax; v++)
	    {
		if (v>0)
		    printf(",");
		printf("%d", Vlen[v]);
	    }
	}
	printf("\n");
    }
}

void usage()
{
    fprintf(stderr, "usage: latclass [-l] [-L] [files]\n"
	    "\n"
	    "Reads stdin if no files are given.\n"
	    "Default is statistics only.\n"
	    "-l lists types of individual lattices.\n"
	    "-L lists types and rank signatures of individual lattices.\n");
    exit(1);
}

void readfile(FILE* in)
{
    graph g[MAXN];
    int m, n;
    boolean digraph;
    
    m = 1;
    n = 0;
    while (readgg(in, g, 1, &m, &n, &digraph))
    {
	lineno++;
	if (!digraph)
	    die("not digraph\n");
	if (n > MAXN)
	    die("graph too big n=%d\n", n);
	if (nseen!=-1 && n+2 != nseen)
	    die("graph wrong size n+2=%d nseen=%d", n+2, nseen);
	nseen = n+2;
	process(n, g);

	if (lineno%1000000 == 0)
	{
	    if (lineno%10000000 == 0)
	        fprintf(stderr, "%lldM\n", lineno/1000000);
	    else
	        fprintf(stderr, ".");
	}
    }
}

int main(int argc, char** argv)
{
    FILE* in;
    int i;
    
    while (argc >= 2 && argv[1][0]=='-')
    {
	switch (argv[1][1])
	{
	case 'l':
	    mode_list = true;
	    break;
	case 'L':
	    mode_list = true;
	    mode_signature = true;
	    break;
	case 'h':
	default:
	    usage(); return 1;
	}
	argc--;
	argv++;
    }

    start = clock();
    lineno = 0;
    if (argc < 2)
    {
	in = stdin;
	readfile(in);
    }
    else
    {
	while (argc >= 2)
	{
	    in = fopen(argv[1], "r");
	    if (!in)
		die("cannot open input file %s", argv[1]);
	    readfile(in);
	    fclose(in);
	    argc--;
	    argv++;
	}
    }
    finish = clock();
    
    fprintf(stderr, "\n");
    printclock();
    fprintf(stderr, " classified in\t%.2f s\n\n",
	    (double)(finish-start)/CLOCKS_PER_SEC);

    long long totalcount = 0;
    lattype typ;
    
    for (typ=lattypefirst; typ<=lattypelast; typ++)
	totalcount += latcount[typ];
    
    if (!mode_list)
    {
	printf("header\tn");
	for (typ=lattypefirst; typ<=lattypelast; typ++)
	    printf("\t%s", lattypes[typ]);
	printf("\ttotal");
	printf("\n");
	if (nseen != -1)
	{
	    printf("counts\t%d", nseen);
	    for (typ=lattypefirst; typ<=lattypelast; typ++)
		printf("\t%lld", latcount[typ]);
	    printf("\t%lld", totalcount);
	    printf("\n");
	}
    }
    
    if (totalcount != lineno)
	die("oops! class totals %lld != line count %lld", totalcount, lineno);
    
    return 0;
}
