
import csv

symtypes = ["sp","mf","ma","mc","mx","mh","bf","bs","tf","ts"]
comtypes = ["cf","cs","cn"]


# Interpred graph6 string as an undirected graph, then convert to directed
def digraph_from_graph6(s):
    G = Graph(s)

    # Take all edges, orient them from bigger to smaller element
    # (towards top), and convert to digraph.
    E = [(max(u,v),min(u,v)) for (u,v,label) in G.edges()]
    D = DiGraph(len(G))
    D.add_edges(E)
    return D


# Read lattice from grpah6 string (top and bottom stripped)
def lattice_from_graph6(s):
    D = digraph_from_graph6(s)
    
    # Bump element numbers up by 1, so we can use 0 for top.
    n      = len(D)
    D.relabel(range(1,n+2))

    # Find coatoms and atoms.
    coats  = D.sinks()
    atoms  = D.sources()

    # Attach top and bottom.
    top    = 0
    bottom = n+1
    D.add_edges((c,top) for c in coats)
    D.add_edges((bottom,a) for a in atoms)
    
    L = LatticePoset(D)
    return L


# Read lattices from graph6 file (top and bottom stripped)
def lattices_from_graph6_file(name):
    for (i,s) in enumerate(open(name,'r')):
        if i>0 and mod(i,1000)==0:
            print("read %d lattices so far" % (i));
        yield(lattice_from_graph6(s))


# Read piece counts from latclass output files.
def paper_read_piece_counts(file):
    reader=csv.reader(open(file), delimiter='\t')
    xx = {typ:[0]*6 for typ in symtypes}
    for row in reader:
        if row[0]=="header" or row[0]=="types":
            0
        elif row[0]=="glued" or row[0]=="counts":
            n = int(row[1])
            for i in range(len(symtypes)):
                typ = symtypes[i]
                val = Integer(row[2+i])
                oldlen = len(xx[typ])
                if oldlen<=n:
                    xx[typ].extend([0] * (n+1-oldlen))
                xx[typ][n] += val
        else:
            raise(ValueError, "invalid row header")
    return xx


def paper_read_modular_counts():
    xx = paper_read_piece_counts("../data/glued.mod")
    # Fill in the small special lattices (n<=3).
    xx["sp"][1] = 1
    xx["sp"][2] = 1
    return xx

def paper_read_distributive_counts():
    xx = paper_read_piece_counts("../data/glued.dist")
    # Fill in the small special lattices (n<=3).
    xx["sp"][1] = 1
    xx["sp"][2] = 1
    return xx

    
# Compute tables of composition counts by symmetry type.
#
# This uses the recurrences from Theorem 1 in the paper.
#
# Input:
#   xx    piece counts by symmetry type: dictionary from type
#         like "mf" to a list of counts
#   nmax  maximum composition size to calculate
#   yy    (optional) initial composition counts -- if provided, recursion starts from here
#   kmax  (optional) max piece size to use, default = use all that we have
#
# Output:
#         composition counts: dictionary from types
#         ("cf", "cs", "cn") to lists of counts
#
# For n <= kmax, the computed counts are exact.
# For n >  kmax, the computed counts are lower bounds, because zeros are used for the unknown piece counts.
#
def paper_cartesian_count(xx, nmax=None, yy=None, kmax=None):
    # Largest available piece size
    if kmax==None:
        kmax = len(xx["mf"])-1
    else:
        kmax = min(kmax, len(xx["mf"])-1)
        
    if nmax==None:
        nmax = kmax
    
    # Shortcuts for input
    sp = xx["sp"]
    mf = xx["mf"]
    ma = xx["ma"]
    mc = xx["mc"]
    mx = xx["mx"]
    mh = xx["mh"]
    bf = xx["bf"]
    bs = xx["bs"]
    tf = xx["tf"]
    ts = xx["ts"]

    # Normally (if no seed provided), initialize smallest entries to zero.
    if yy==None:
        yy = {"cf":[0]*6, "cs":[0]*6, "cn":[0]*6}

    # Shortcuts for composition counts
    cf = yy["cf"]
    cs = yy["cs"]
    cn = yy["cn"]
    
    nmin = len(yy["cf"])
    for n in range(nmin, nmax+1):
        # print("Computing for n=%d" % (n))
        cfn = 0
        csn = 0
        cnn = 0
        totaln = 0
        
        if kmax==None:
            kmaxhere = n-2
        else:
            kmaxhere = min(kmax, n-2)
            
        for k in range(6, kmaxhere+1):
            j = n-k+4

            if j>kmax:
                lfj = cf[j]
                lsj = cs[j]
            else:
                lfj = cf[j] + bf[j] + mf[j] + ma[j]
                lsj = cs[j] + bs[j] + mc[j] + mx[j] + mh[j]
                
            cfn += lfj*(2*mf[k]+ma[k]+mh[k]) + lsj*(mf[k]+ma[k])
            csn += lfj*(2*mc[k]+mx[k]) + lsj*(mc[k]+mx[k]+mh[k])
            cnn += lfj*(2*tf[k]+ts[k]) + lsj*(tf[k]+ts[k])
        yy["cf"].append(cfn)
        yy["cs"].append(csn)
        yy["cn"].append(cnn)

    return yy


def paper_total_vi(xx, nmax):
    kmax = len(xx["mf"])-1
    yy = paper_cartesian_count(xx, nmax)
    vi = [0]*(nmax+1)
    for n in range(1,nmax+1):
        vi[n] = yy["cf"][n] + yy["cs"][n] + yy["cn"][n]
        if n<=kmax:
            for typ in symtypes:
                vi[n] += xx[typ][n]
    return vi


# Verify an exponential lower bound on CF, CS and CN.
#
# An exponential lower bound is of the form a*c^n
# for n >= nmin.
#
# Input:
#   xx           piece counts by symmetry type: dictionary from type to list of counts
#   acf,acs,acn  claimed coefficients (for each symmetry type)
#   c            claimed base of the exponential (common to the three types)
#   nmin         claimed bound should hold for n>=nmin
#   extra        number of extra indices to verify explicitly (above nmin)
#
# All inputs should be integer or rational (not float), to ensure exact arithmetic.
#
def paper_lower_bound_verify(xx, acf, acs, acn, c, nmin, extra=0):
    # Largest available piece size
    k = len(xx["mf"])-1
    print("Using pieces up to k=%d" % (k))

    dirok = True
    indok = True
    okfail = ["FAIL", "OK"]
    
    # First part. Verify from nmin to nmin+k+extra by direct calculation.
    print("Direct calculation up to n=%d" % (nmin+k+extra))
    yy = paper_cartesian_count(xx, nmin+k+extra)
    for n in range(nmin, nmin+k+extra+1):
        cfn = yy["cf"][n]
        csn = yy["cs"][n]
        cnn = yy["cn"][n]
        cfexp = acf * c^n
        csexp = acs * c^n
        cnexp = acn * c^n
        thisok = (cfn >= cfexp) and (csn >= csexp) and (cnn >= cnexp)
        if not thisok:
            dirok  = False
        if n==nmin or mod(n,10)==0 or not(thisok):
            print("n=%4d   %.6f  %.6f  %.6f   %s" % (n, cfn/cfexp, csn/csexp, cnn/cnexp, okfail[thisok]))
    print("Direct calculation %s" % (okfail[dirok]))

    # Second part. Verify for n>nmin+k by induction.
    # This is explicitly done for n=nmin+k+1 and all other cases are similar.
    # We create ASSUMED lower bounds zz on composition counts from nmin to nmin+k,
    # assuming the induction hypothesis.
    print("")
    print("Induction step")
    zz = {}
    nvec = range(nmin, nmin+k+1)
    nnext = nmin+k+1
    zz["cf"] = [0]*nmin + [acf * c^n for n in nvec]
    zz["cs"] = [0]*nmin + [acs * c^n for n in nvec]
    zz["cn"] = [0]*nmin + [acn * c^n for n in nvec]
    # Then we verify that the induction hypothesis implies the claim also for nmin+k+1.
    zznext = paper_cartesian_count(xx, nnext, zz)
    cfnext = zznext["cf"][nnext]
    csnext = zznext["cs"][nnext]
    cnnext = zznext["cn"][nnext]
    cfexp  = acf * c^nnext
    csexp  = acs * c^nnext
    cnexp  = acn * c^nnext
    thisok = (cfnext >= cfexp) and (csnext >= csexp) and (cnnext >= cnexp)
    if not thisok:
        indok  = False
    print("n=%4d   %.6f  %.6f  %.6f   %s" % (nnext, cfnext/cfexp, csnext/csexp, cnnext/cnexp, okfail[indok]))
    print("  cf_n >= %.9f * c^n" % (cfnext / c^nnext))
    print("  cs_n >= %.9f * c^n" % (csnext / c^nnext))
    print("  cn_n >= %.9f * c^n" % (cnnext / c^nnext))
    print("Induction step %s" % (okfail[indok]))

    print("")
    print("Total of claimed coefficients = %.6f" % (acf+acs+acn))
    
    return dirok and indok


def paper_lower_bound_verify_modular(xx, extra=0):
    return paper_lower_bound_verify(xx, 2910/1000000, 35/1000000, 2470/1000000, 23122/10000, 50, extra=extra)

def paper_lower_bound_verify_distributive(xx, extra=0):
    return paper_lower_bound_verify(xx, 10600/1000000, 92/1000000, 1950/1000000, 17250/10000, 100, extra=extra)


# Count all lattices in a family using the vertical-sum recurrence rule (1),
# from the numbers of vi-lattices in the same family.
#
# Input is a list of counts of vi-lattices.
#
# Optional input seed is is a prefix of assumed total counts, recursion then starts from there.
# If not provided, recursion starts from 2.
def paper_v1_cartesian_count(vi, nmax, seed=None):
    kmax = len(vi)-1
    if seed==None:
        total = [0]*(nmax+1)
        total[1] = 1                # Singleton lattice is assumed to be in family
        nmin = 2
    else:
        total = seed + [0]*(nmax+1-len(seed))
        nmin = len(seed)
        
    for n in range(nmin, nmax+1):
        kmaxhere = min(kmax, n)
        for k in range(2, kmaxhere+1):
            total[n] += vi[k] * total[n-k+1]
    return total

def paper_v1_lower_bound_verify(xx, a, c, nmin, k, extra=0):
    # Compute surely enough vi lower bounds
    vi = paper_total_vi(xx, 2*(nmin+k))
    
    dirok = True
    indok = True
    okfail = ["FAIL", "OK"]
    
    # Direct calculation
    print("Direct calculation up to n=%d" % (nmin+k+extra))
    total = paper_v1_cartesian_count(vi, nmin+k+extra)
    for n in range(nmin, nmin+k+extra+1):
        totaln   = total[n]
        totalexp = a * c^n
        thisok = (totaln >= totalexp)
        if not thisok:
            dirok  = False
        if mod(n,10)==0 or not(thisok):
            print("n=%4d   %.6f   %s" % (n, totaln/totalexp, okfail[thisok]))
    print("Direct calculation %s" % (okfail[dirok]))
    
    # Induction step.
    nnext = nmin+k
    print("Induction step at n=%d" % (nnext))
    # Set up the induction assumption at nmin+k: We are assuming the claim holds from nmin to nmin+k.
    # Below nmin we assume nothing, so set those to zeros.
    nprev = range(nmin, nmin+k+1)
    totalprev = [0]*nmin + [a*c^n for n in nprev]
    # Verify that the claim then holds for nmin+k+1
    nnext = nmin+k+1
    zznext = paper_v1_cartesian_count(vi[0:nnext], nnext, totalprev)[nnext]
    zzclaim = a*c^nnext
    thisok = (zznext >= zzclaim)
    if not thisok:
        indok = false;
    print("n=%4d   %.6f   %s" % (nnext, zznext/zzclaim, okfail[indok]))
    print("  zz_n >= %.9f * c^n" % (zznext / c^nnext))
    print("Induction step %s" % (okfail[indok]))
    return dirok and indok


def paper_v1_lower_bound_verify_modular(xx, extra=0):
    return paper_v1_lower_bound_verify(xx, 20/1000, 23713/10000, 100, 300, extra=extra)

def paper_v1_lower_bound_verify_distributive(xx, extra=0):
    return paper_v1_lower_bound_verify(xx, 88/1000, 18433/10000, 100, 300, extra=extra)

def ratio(X, floatconvert=True):
    if floatconvert:
        return [float(X[i+1]/X[i]) for i in range(len(X)-1)]
    else:
        return [X[i+1]/X[i] for i in range(len(X)-1)]
